#!/bin/bash
# A sample Bash script



#1. build the app and create two different apks
gradle assembleDebug
gradle assembleAndroidTest


#2. run unit tests
gradle test

#gradle jacocoTestReport
gradle createDebugCoverageReport

#3. create a virtual device using avdmanager and list the avds
avdmanager create avd -n 3.7_WVGA_Nexus_One_API_26 -d 27  -k 'system-images;android-27;google_apis;x86'
avdmanager list avd


#4. adb versatile command-line tool that lets you communicate with a device. kill the server part of the adb prog
adb kill-server #restart sever process

#.5. enable emulator debug logs
export ANDROID_EMULATOR_DEBUG=1 3.7_WVGA_Nexus_One_API_26


#6. change dir to the emulator dir and run the device using emulator cmd don not use the default one located in tools
cd '../android-sdk-linux/emulator'
pwd

#7. start the server which will open connections on port 0.0.0.0:5037
adb start-server


#8. If you want to do something after you start the emulator you should start it in the background wait and start shell when boot is complete
./emulator  -avd 3.7_WVGA_Nexus_One_API_26 -noaudio -no-boot-anim -gpu off -no-window & 
adb wait-for-device shell 'while [[ -z $(getprop sys.boot_completed) ]]; do sleep 1; done;'

#9. check avds listed 
./emulator  -list-avds

#10. Before issuing adb commands, it is helpful to know what device instances are connected to the adb server. 
adb devices

#11. Go back to the workspace and install both apks onto the started devices
cd '../../workspace'
pwd





#12. install both apks
gradle installDebug installDebugAndroidTest


# before you run your tests set up a redirection 
# To communicate with an emulator instance behind its virtual router, you need to set
# up network redirection on the virtual router. Clients can then connect to a specified guest
# port on the router, while the router directs traffic to/from that port to the emulated device host port.


#Next, connect to the console of the target emulator instance, 
#telnet localhost 5554
#redir add tcp:5222:6000

#set target device will listen to connection on that part
#adb tcpip 5554
adb forward tcp:5222 tcp:6000
adb forward --list

#adb logcat CONN:D *:S



#13. run the instrumented tests that are large
#gradle app:connectedAndroidTest -Pandroid.testInstrumentationRunnerArguments.size=large
gradle app:connectedAndroidTest -P android.testInstrumentationRunnerArguments.class=com.creativityinmotion.hoolu_messenger_app.HooluMessageXMPPTest#connectIfUserIsValid



