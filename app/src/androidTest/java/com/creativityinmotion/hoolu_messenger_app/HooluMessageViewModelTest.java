package com.creativityinmotion.hoolu_messenger_app;


import android.arch.core.executor.testing.InstantTaskExecutorRule;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProvider;
import android.support.test.filters.LargeTest;
import android.support.test.filters.MediumTest;

//import com.creativityinmotion.hoolu_messenger_app.view.MessageFragment;
import com.creativityinmotion.hoolu_messenger_app.viewmodel.MessageActivityViewModel;
import com.uzaal.chat.usecases.UseCase;
import com.uzaal.chat.usecases.chat.GetMessages;
import com.uzaal.chat.usecases.chat.SendMessage;
import com.uzaal.chat.usecases.entities.HooluMessage;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.ArgumentMatchers;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;


//import static org.hamcrest.CoreMatchers.any;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;


/**
 * Created by root on 6/22/18.
 */


@RunWith(MockitoJUnitRunner.class)
@MediumTest
public class HooluMessageViewModelTest {

     //msg limit = 256 chars
     //When Testing the UI you verify certain UI methosds were called apropriately
     @Rule
     public InstantTaskExecutorRule instantTaskExecutorRule = new InstantTaskExecutorRule();


    @Mock
    ViewModelProvider viewModelProvider;

    @Mock
    Observer<CharSequence> messageObserver;

    @Mock
    Observer<Boolean> isMessageReceivedObserver;

    @Mock
    Observer<Boolean> isMessageSentObserver;

    @Mock
    Observer<String> messageErrorObserver;

    @Mock
    Observer<List<HooluMessage>> messageListObserver;


   // MessageFragment messageFragment;

    @Mock
    SendMessage sendMessage;

    @Mock
    GetMessages getMessages;

    // @Mock
    // UseCase.OutputBoundary outputBoundary;


    @InjectMocks
    MessageActivityViewModel viewModel;



    // {@link ArgumentCaptor} is a powerful Mockito API to capture argument values and use them to
    // perform further actions or assertions on them.




    @Captor
    private ArgumentCaptor<UseCase.OutputBoundary<SendMessage.ResponseValue>> sendMsgOutputArgumentCaptor;
    @Captor
    private ArgumentCaptor<UseCase.OutputBoundary<GetMessages.ResponseValue>> getMsgOutputArgumentCaptor;


    ArrayList<HooluMessage> dummyHooluMessages;
    HooluMessage dummyHooluMessage;

    @Before
    public void setUp(){

        MockitoAnnotations.initMocks(this);


       // messageFragment = new MessageFragment();

        dummyHooluMessage = new HooluMessage();
        dummyHooluMessage.setUserID("1");
        dummyHooluMessage.setMessage("Hello guys");

        dummyHooluMessages = new ArrayList<>();
        dummyHooluMessages.add(dummyHooluMessage);

      //  messageFragment.setViewModelProvider(viewModelProvider);

        when(viewModelProvider.get(any(Class.class))).thenReturn(viewModel);

       // when(viewModel.getmSentMessage()).thenReturn(mSentMessage);
        //when(viewModel.getmReceivedMessage()).thenReturn(mReceivedMessage);
        //when(viewModel.getmMessageConversations()).thenReturn(mConversations);

    }



    //Qstn Does the UI actually show the message after a trigger or you specified a wrong observer
    //to a UI element

    //valid data between 1 - 5
    //invalid > 256   and < 1 or empty



    /**
      TC1 - verify send msg with valid data (btn 1 - 256 chars)
      Test Scenario -  send message successfully -> basic flow
      Preconditions -  both parties online
      Test Steps    - 1. enter msg,  2. click send button
      TestData      - ("Hello friend")
      Expected      - the UI list is updated msg appears, 1 tick appears
     */
    @Test
    public void shouldUpdateUIwithSentMessageAfterMessageIsSentSuccessfully() {
          //should update ui with sent msg if message is sent

        // When msg viewmodel is asked to receive the msg
        viewModel.setmMessage("Hello friend");
        viewModel.getmMessage().observeForever(messageObserver); //the UI is the observer
        verify(messageObserver).onChanged(anyString());

        //Does the viewModel sendMessage method invoke the right methods in use case e.g execute?
        //If it does then its good to go
        //1. And When the presenter/view model is asked to send an existing message

          //viewModel.setPreferences();
          viewModel.sendmMessage("hi");

        //2. Add the message to the UI list
        viewModel.getMessageList().observeForever(messageListObserver);
        verify(messageListObserver).onChanged(ArgumentMatchers.<HooluMessage>anyList());

        //2. Then the use case is executed
        verify(sendMessage, Mockito.times(1)).execute(any(SendMessage.RequestValues.class), sendMsgOutputArgumentCaptor.capture());


        HooluMessage hooluMessage = new HooluMessage();
        hooluMessage.setMessage("Hello guys");
        hooluMessage.setDeliveryStatus(HooluMessage.DeliveryStatus.TO);
        hooluMessage.setFrom("piero");

        //3. if executed then Simulate callback to trigger updating messages in the UI //pass in dummy response value
        sendMsgOutputArgumentCaptor.getValue().onSuccess(new SendMessage.ResponseValue(hooluMessage));


        //4. verify that get msg use case executed after the success simulated trigger
            // verify(getMessages).execute(any(GetMessages.RequestValues.class), getMsgOutputArgumentCaptor.capture());


        //5. if executed Simulate callback to trigger and make sure our view updates as expected with our dummy messages
            //  getMsgOutputArgumentCaptor.getValue().onSuccess(new GetMessages.ResponseValue(dummyHooluMessages));


        //6. check whether message tick was invoked
           viewModel.getIsMessageSent().observeForever(isMessageSentObserver);
           verify(isMessageSentObserver).onChanged(true);

         //Was the msg really received thus does the isSentMsg observer get triggered coz the message was sent ?
         //viewModel.ismMessageReceived.observeForever(isMessageReceivedObserver);
         //verify(isMessageReceivedObserver).onChanged(anyBoolean());




    }



    /**
     TC2 - verify send msg with invalid data empty message (" ")
     Test Scenario -  basic flow -> sys detects invalid message -> error msg is displayed
     Preconditions -  both parties online
     Test Steps    - 1. enter msg,  2. click send button
     TestData      - ("") - empty string
     Expected      -  the UI list is not updated, error flag shown
     */

    @Test
    public void shouldShowErrorifMessageIsInvalid() {


        // Pretend the message view model is triggered
        viewModel.setmMessage("");
        viewModel.getmMessage().observeForever(messageObserver); //the UI is the observer
        verify(messageObserver).onChanged(anyString());

        // then when send msg is asked to send a msg/empty msg thats to say
        viewModel.sendmMessage("hi");

        //and message is empty
        //2. the usecase should attempt to send the msg
        verify(sendMessage, Mockito.times(1)).execute(any(SendMessage.RequestValues.class), sendMsgOutputArgumentCaptor.capture());


        //3. if send msg is invoked then Simulate callback to trigger invalid message UI/notification
        sendMsgOutputArgumentCaptor.getValue().onInvalidMessage();


        //error is triggered
        viewModel.getmError().observeForever(messageErrorObserver);
        verify(messageErrorObserver).onChanged(anyString());

    }


    /**
     TC3 - verify send msg with invalid data exceeding limit (btn 1 - 256 chars)
     Test Scenario -  basic flow -> sys detects invalid message -> error msg is displayed
     Preconditions -  both parties online
     Test Steps    - 1. enter msg,  2. click send button
     TestData      - ("Hello friend how are you") - max string 10 chars
     Expected      -  the UI list is not updated, error flag shown
     */
    @Test
    public void tc03_verify_sendMessageWithInValidLongData_flags() {

        viewModel.setmMessage("Hello friend how are you, hope your okay");
        viewModel.getmMessage().observeForever(messageObserver); //the ui is the observer
        verify(messageObserver).onChanged(anyString());


        viewModel.sendmMessage("helo");
        viewModel.getMessageList().observeForever(messageListObserver);
        verify(messageListObserver, never()).onChanged(ArgumentMatchers.<HooluMessage>anyList());

        //the usecase executed impl

       // viewModel.ismMessageReceived.observeForever(isMessageReceivedObserver);
        verify(messageErrorObserver, never()).onChanged(anyString());

        //viewModel.ismMessageSent.observeForever(isMessageSentObserver);
        verify(messageErrorObserver, never()).onChanged(anyString());


        viewModel.getmError().observeForever(messageErrorObserver);
        verify(messageErrorObserver).onChanged(anyString());



    }



    /**
     TC3 - verify send msg with valid data while offline (btn 1 - 256 chars)
     Test Scenario -  basic flow -> sys detects no network -> error msg is displayed
     Preconditions -  sender is offline
     Test Steps    - 1. enter msg,  2. click send button
     TestData      - ("Hello friend ")
     Expected      -  the UI list is not updated, error flag shown
     */
    @Test
    public void tc04_verify_sendMessageWithValidData_whileNoNetwork_flags() {

        viewModel.setmMessage("Hello friend");
        viewModel.getmMessage().observeForever(messageObserver); //the ui is the observer
        verify(messageObserver).onChanged(anyString());


        viewModel.sendmMessage("how that");
        viewModel.getMessageList().observeForever(messageListObserver);
        verify(messageListObserver).onChanged(ArgumentMatchers.<HooluMessage>anyList()); //list update


        //the usecases should have executed

        //when offline the msg shldnt be received so never triggered
        //when offline the msg shldnt be sent so never triggered

       // viewModel.ismMessageReceived.observeForever(isMessageReceivedObserver);
        verify(messageErrorObserver, never()).onChanged(anyString());

        //viewModel.ismMessageSent.observeForever(isMessageSentObserver);
        verify(messageErrorObserver, never()).onChanged(anyString());


        viewModel.getmError().observeForever(messageErrorObserver);
        verify(messageErrorObserver).onChanged(anyString());



    }









   /*






    //TC3 - verify send msg with invlaid max char
    @Test
    public void should_ShowFlagWithInValidDataMoreThanLimit() {

        //TestData (6)
        //valid data between 1 - 5
        //invalid > 256   and < 1 or emptyss

        // assertEquals(model.getMessages().size(), 0);
        when(model.getMessages().size()).thenReturn(0);
        model.setInputTextMessage("hello whats your name");
        model.sendMessage();
        //assertEquals(model.getMessages().size(), 0);
        when(model.getMessages().size()).thenReturn(0);
        verify(model).showError();

    }




    //TC4 - verify send msg with invlaid max char
    @Test
    public void should_ShowFlagWithDataJustAboveLimit() {
        //valid data between 1 - 5
        //invalid > 256   and < 1 or empty

        //TestData (6 chars)




    }


    //TC5 - verify send msg with invlaid max char
    @Test
    public void check_SendMessageWithDataJustBelowLimit() {

        //Test Scenario -  send message
        //preconditions -  contact available, valid data
        //Test Steps    - 1. enter msg,  2. click send button
        //TestData (4 chars)
        //valid data between 1 - 5
        //invalid > 256   and < 1 or empty

        //expected result



    }


     */




}
