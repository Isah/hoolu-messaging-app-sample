package com.creativityinmotion.hoolu_messenger_app;

import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.support.test.InstrumentationRegistry;
import android.support.test.filters.LargeTest;
import android.support.test.rule.ServiceTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.util.Log;


import com.uzaal.chat.usecases.chat.ChatSmackService;
import com.uzaal.chat.usecases.chat.ChatSmackServiceClient_Factory;
import com.uzaal.chat.usecases.login.LoginSmackService;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;

import java.util.concurrent.TimeoutException;

import static junit.framework.Assert.assertFalse;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

/**
 * Created by root on 6/21/18.
 */




@RunWith(AndroidJUnit4.class)
//@RunWith(MockitoJUnitRunner.class)
@LargeTest
public class HooluMessageBoundServiceTest {


    //Android Test provides an API for testing your Service objects in isolation.
    //The ServiceTestRule class is a JUnit 4 rule that starts your service before
    // your unit test methods run, and shuts down the service after tests complete.
    // By using this test rule, you ensure that the connection to the service is
    // always established before your test method runs.


    @Rule
    public final ServiceTestRule mServiceRule = new ServiceTestRule();



    Intent serviceIntent;

    @Before
    public void startService() {


        serviceIntent  = new Intent(InstrumentationRegistry.getTargetContext(), ChatSmackService.class);

        // Data can be passed to the service via the Intent.
        serviceIntent.putExtra("usr", "piero@localhost");
        serviceIntent.putExtra("pwd", "piero");
        serviceIntent.putExtra("host", "localhsot");
        serviceIntent.putExtra("host_address", "10.0.2.1");

    }


    @After
    public void stopService() {

        mServiceRule.unbindService();

    }

    /**
     *OnCreate has to be called if the service is bound
     *
     * @throws TimeoutException
     */
    @Test
    public void boundServiceToClientIfStarted() throws TimeoutException {
        // Create the service Intent

        // Bind the service and grab a reference to the binder. e service;
        IBinder binderService = mServiceRule.bindService(serviceIntent);

        ChatSmackService service = ( (ChatSmackService.LocalBinder) binderService).getService();

        assertTrue(service.started);


    }

    /**
     *OnDestroy has to be called if the service is unbound
     *
     * @throws TimeoutException
     */
    @Test
    public void unBoundServiceFromClientIfStopped() throws TimeoutException {
        // Create the service Intent

        IBinder binderService = mServiceRule.bindService(serviceIntent);

        ChatSmackService service = ((ChatSmackService.LocalBinder) binderService).getService();

        if(service.started){

            mServiceRule.unbindService();

            assertTrue(service.stopped);

        }



    }




    boolean loggedIn = false;

    @Test
    public void invokeXmppLoginIfRequested() throws TimeoutException {
        // Create the service Intent

        // Bind the service and grab a reference to the binder. e service;
        IBinder binderService = mServiceRule.bindService(serviceIntent);

        LoginSmackService service = ( (LoginSmackService.LocalBinder) binderService).getService();



        /*
        service.logInToXmpp(new MessageDataSource.LogInCallback() {
            @Override
            public void onUserLogIn() {

                loggedIn = true;

            }

            @Override
            public void onLogInFailed() {

                loggedIn = true;

            }
        });


         assertTrue(loggedIn);
         */

    }


    //invoke send message
    //invoke






}