package com.creativityinmotion.hoolu_messenger_app;

import android.support.test.espresso.Espresso;
import android.support.test.espresso.matcher.ViewMatchers;
import android.support.test.filters.LargeTest;
import android.support.test.filters.MediumTest;
import android.support.test.runner.AndroidJUnit4;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.hasDescendant;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import android.support.test.rule.ActivityTestRule;
import android.support.v7.widget.RecyclerView;

/**
 * Created by root on 6/21/18.
 */



@RunWith(AndroidJUnit4.class)
@MediumTest
public class HooluMessageScreenTest {


    @Rule
    public ActivityTestRule<MainActivity> mActivityRule = new ActivityTestRule<>(
            MainActivity.class);


    // Convenience helper
    public static RecyclerViewMatcher withRecyclerView(final int recyclerViewId) {
        return new RecyclerViewMatcher(recyclerViewId);
    }


    //Scenario- user sends msg net available msg sent tick
    //This is an end to end test
   @Test
   public void AddMessageToConversationList(){

       // msg size, empty (valid,invalid)
       onView(ViewMatchers.withId(R.id.et_enterMessage)).perform(typeText("Hello"));
       onView(withId(R.id.btn_sendMessage)).perform(click());


       // Check item at position 3 has "Some content"
       onView(withRecyclerView(R.id.recycler_view_messages).atPosition(0))
               .check(matches(hasDescendant(withText("Hello"))));

       // Click item at position 3
     //  onView(withRecyclerView(R.id.recycler_view_messages).atPosition(0)).perform(click());


       /*
       // Check that the item has the special text.
       String middleElementText = mActivityRule.getActivity().getResources().getString(R.string.received);
       onView(withText(middleElementText)).check(matches(isDisplayed()));
       */


   }









}
