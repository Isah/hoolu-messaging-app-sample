package com.creativityinmotion.hoolu_messenger_app;

import android.os.AsyncTask;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.test.filters.LargeTest;
import android.support.test.runner.AndroidJUnit4;
import android.util.Log;


import com.creativityinmotion.hoolu_messenger_app.utilities.AppExecutors;


import org.jivesoftware.smack.SmackException;
import org.jivesoftware.smack.StanzaListener;
import org.jivesoftware.smack.chat2.Chat;
import org.jivesoftware.smack.chat2.ChatManager;
import org.jivesoftware.smack.chat2.IncomingChatMessageListener;
import org.jivesoftware.smack.packet.Message;
import org.jivesoftware.smack.packet.Stanza;
import org.jivesoftware.smackx.ping.PingFailedListener;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.jxmpp.jid.EntityBareJid;
import org.jxmpp.jid.EntityJid;
import org.jxmpp.jid.impl.JidCreate;
import org.jxmpp.stringprep.XmppStringprepException;

import java.util.concurrent.Executor;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.hamcrest.CoreMatchers.is;


import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;

/**
 * Created by root on 6/21/18.
 */
@RunWith(AndroidJUnit4.class)
public class HooluMessageXMPPTest {



  //  @Rule
   // public ActivityTestRule<MainActivity> mActivityRule = new ActivityTestRule<>(MainActivity.class);

   // @Mock
    //MyXMPP.MyXMPPConnectionListener connectionListener;



   // MyXMPP xmpp;


    StanzaListener packetListener;
    PingFailedListener pingFailedListener;


    AppExecutors appExecutors;

    @Before
    public void startXMPPService(){

     //   MockitoAnnotations.initMocks(this);

       // xmpp  = MyXMPP.getInstance();

        appExecutors = new AppExecutors();


    }




    @After
    public void closeConnection(){
   // xmpp.disconnectConnection();

    }

    Stanza actualStanza;
    String actualJid = "";

    private  boolean isPinged;

    Runnable runnable;

    //Scenario- user sends msg net available msg sent tick
    //This is an end to end test
   @Test
   public void connectIfUserIsValid(){// expectedoutcome-scenario


       /*

       String userName = "petr";
       String passWord = "petr";
       String host = "localhost";
       String hostAddressLocalhost = "10.0.2.2"; //its alias to the computers local host
       String hostAddress = "172.21.0.2"; //address of the docker contianer of ejabberd

       Log.d("CONN: ", "Init");

       //full user: jid = piero@localhost
       xmpp.init(userName, passWord, host, hostAddressLocalhost,15000);



         Log.d("CONN: ", "STARTED Asserting");

        Looper.prepare();
       final Handler handler = new Handler();
       handler.postDelayed(new Runnable() {
           @Override
           public void run() {


               Log.d("CONN: ", "ASSERT STARTED"+xmpp.isConnected());


               assertTrue(xmpp.isConnected());

               Looper myLooper = Looper.myLooper();
               if (myLooper!=null) {
                   myLooper.quit();
               }


           }
       }, 10000);
       Looper.loop();


     */


   }



   /*
    @Test
    public void sendMessageIfUserIsValid(){

       String userName = "piero";
       String passWord = "piero";
       String host = "localhost";
       String hostAddressLocalhost = "10.0.2.2"; //its alias to the computers local host
       String hostAddress = "172.21.0.2"; //address of the docker contianer of ejabberd


        //full user: jid = piero@localhost

        xmpp.init(userName, passWord, host, hostAddressLocalhost,15000);


        Looper.prepare();
       final Handler handler = new Handler();
       handler.postDelayed(new Runnable() {
           @Override
           public void run() {

                  if(xmpp.isConnected() && xmpp.login()) {

                    Message message = new Message();
                    message.setBody("Hello Xmpp im In");
                    message.setSubject("Just Chatting");

                    xmpp.sendMsg(message);

                      Log.d("MESSAGE: ", "SENT"+xmpp.isConnected());


                      xmpp.addMessageListener(new IncomingChatMessageListener() {
                        @Override
                        public void newIncomingMessage(EntityBareJid from, Message message, Chat chat) {

                            Log.d("MESSAGE", message.getBody());

                            String expectedMessage = "Hello Xmpp im In";

                            assertEquals(expectedMessage,message.getBody());

                        }
                    });

                }


               Looper myLooper = Looper.myLooper();
               if (myLooper!=null) {
                   myLooper.quit();
               }


           }
       }, 5000);
       Looper.loop();


    }

    */




   public class MainThreadExecutor implements Executor{

       private Handler mainThreadHandler = new Handler(Looper.getMainLooper());

       @Override
       public void execute(@NonNull Runnable runnable) {

           mainThreadHandler.postDelayed(runnable, 5000);
       }
   }






    @Test
    public void logInIfConnectedAndUserValid(){

       /*
        String userName = "petrl";
        String passWord = "petr";
        String host = "localhost";
        String hostAddress = "172.21.0.2"; //its alias to the computers local host
        String hostAddressLocal = "10.0.2.2"; //its alias to the computers local host

        //full user: jid = piero@localhost

        xmpp.init(userName, passWord, host, hostAddressLocal,15000);

        assertTrue(xmpp.login());
*/
    }





    EntityJid bareJid;


   /*
    public void sendMessage() {

       ChatManager chatManager = ChatManager.getInstanceFor(xmpp.getConnection());

       chatManager.addIncomingListener(new IncomingChatMessageListener() {
           @Override
           public void newIncomingMessage(EntityBareJid from, Message message, Chat chat) {





           }
       });


        Chat chat;

       try {

         bareJid =  JidCreate.entityBareFrom("piero@localhost");


       }catch (XmppStringprepException e){


       }


        Log.d("SEND MESSAGE BEFORE EXECUTE", bareJid.toString());


        if(bareJid!=null){

            Log.d("SEND MESSAGE READY EXECUTE", bareJid+toString());


            chat = chatManager.chatWith(bareJid.asEntityBareJid());


           final Message message = new Message();
           message.setBody("Its me Hello!");
           //message.setStanzaId();
           message.setType(Message.Type.chat);

           try {


               chat.send(message);



           } catch (SmackException.NotConnectedException | InterruptedException e) {
               e.printStackTrace();
           }



       }







    }

    */






   @After
   public void After(){
//    xmpp.stop();

   }


    boolean isConnected = false;
    int connectionCount = 0;
    public boolean recurseConnectionCheck(){

         /*
            if (!isConnected) {

                Log.d(" CONNECTION THREAD", "sleep finished: connection state is: " + isConnected);

                isConnected = xmpp.isConnected();

                connectionCount++;
                recurseConnectionCheck();


            }


            Log.d(" CONNECTION THREAD", "sleep finished: connection state is: " + isConnected);

        return isConnected;
        */

         return true;
    }









}
