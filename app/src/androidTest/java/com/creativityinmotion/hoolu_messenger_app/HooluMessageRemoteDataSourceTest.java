package com.creativityinmotion.hoolu_messenger_app;

import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Handler;
import android.os.Looper;
import android.support.test.InstrumentationRegistry;
import android.support.test.espresso.idling.CountingIdlingResource;
import android.support.test.filters.MediumTest;


import com.uzaal.chat.usecases.chat.ChatDataSource;
import com.uzaal.chat.usecases.chat.ChatSmackService;
import com.uzaal.chat.usecases.config.PreferenceHandler;
import com.uzaal.chat.usecases.entities.HooluMessage;
import com.uzaal.chat.usecases.login.LoginServiceClient;
import com.uzaal.chat.usecases.login.LoginSmackService;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.concurrent.TimeoutException;

import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;

/**
 * Created by root on 6/21/18.
 */





@RunWith(MockitoJUnitRunner.class)
@MediumTest
public class HooluMessageRemoteDataSourceTest {

    //Android Test provides an API for testing your Service objects in isolation.
    //The ServiceTestRule class is a JUnit 4 rule that starts your service before
    // your unit test methods run, and shuts down the service after tests complete.
    // By using this test rule, you ensure that the connection to the service is
    // always established before your test method runs.

    @Mock
    ChatSmackService.OnConnectionEstablishedListener onConnectionEstablishedListener;

    @Mock
    ChatDataSource.ConnectionCallback connectionCallback;

    @Captor
    ArgumentCaptor<ChatDataSource.ConnectionCallback> connectionCallbackArgumentCaptor;


    @Mock
    ChatDataSource.SendMessageCallback sendMessageCallback;

    @Mock
    ChatSmackService binderService;

    @Mock
    LoginSmackService loginBinderService;

    @Captor
    ArgumentCaptor<LoginServiceClient.LogInCallback> loginServiceConnectionArgumentCaptor;



    @Mock
    ServiceConnection serviceConnection;

    @Captor
    ArgumentCaptor<ServiceConnection> serviceConnectionArgumentCaptor;


   // @InjectMocks
    //MessageRemoteDataSource messageRemoteDataSource;


    PreferenceHandler preferenceHandler;
    HooluMessage dummyHooluMessage;




    Context binderContext;



    @Before
    public void setUp() {

        dummyHooluMessage = new HooluMessage();
        dummyHooluMessage.setFrom("Peter");
        dummyHooluMessage.setMessage("Hi");

        binderContext = InstrumentationRegistry.getTargetContext();

        //MyApplication.get(InstrumentationRegistry.getTargetContext()).getApplicationComponent().;

           //binding is called on the constructor



      //  messageRemoteDataSource = new MessageRemoteDataSource(preferenceHandler,binderContext);

    }


    @After
    public void stop() {

    }

    //1. make sure the class's binder service was called after initialisation
    //2. capture the service connection
    //3. and on conectionEstablished verify other behaviors
    //4. e.g try login in and verify xmpp login mtd called


    /*

    @Test
    public void bindServiceAfterRepositoryInitialisation(){
        // Create the service Intent
        final Intent intent = new Intent(binderContext, ChatSmackService.class);
        intent.putExtra("usr","peiro");
        intent.putExtra("pwd","isah");
        intent.putExtra("host","host");
        intent.putExtra("host_address","address");

        // Mockito.verify(application).bindService(any(eq(Intent.class)),any(ServiceConnection.class),Context.BIND_AUTO_CREATE);
        // any(eq(Intent.class)),any(ServiceConnection.class)
        // Mockito.verify(binderService).bindService(any(eq(Intent.class)),any(ServiceConnection.class),anyInt());
        // Mockito.verify(binderService,Mockito.atLeast(1)).getPackageName();

        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        Mockito.verify(binderService).bindService(intent,serviceConnection,0);



    }


    CountingIdlingResource countingIdlingResource = new  CountingIdlingResource("SERVICE");


    @Test
    public void establishConnectionAfterServiceIsStarted() throws TimeoutException {
        //i dont care if the service got bound i just need to simulate that it got bound

        Looper.prepare();
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {

              //  Log.d("CONN: ", "ASSERT STARTED"+xmpp.isConnected());
              //--  Mockito.verify(binderService).makeConnectionToXmpp(connectionCallback);

                Looper myLooper = Looper.myLooper();
                if (myLooper!=null) {
                    myLooper.quit();
                }

            }
        }, 5000);
        Looper.loop();
    }


    @Test
    public void invokeLoginByServiceIfConnected() throws TimeoutException {

        Looper.prepare();
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {

                //  if a connection to xmpp has been called
                Mockito.verify(loginBinderService).connectLogin(loginServiceConnectionArgumentCaptor.capture());

                //  executed simulate the callback for a connection
                loginServiceConnectionArgumentCaptor.getValue().onUserLogIn();

                // to check whether if i try to invoke a login, it executes if its connected
                // and doesnt execute if its not connected
               // loginBinderService.logInToXmpp(logInCallback);
               // Mockito.verify(loginBinderService).logInToXmpp(logInCallback);



                Looper myLooper = Looper.myLooper();
                if (myLooper!=null) {
                    myLooper.quit();
                }


            }
        }, 5000);
        Looper.loop();



    }


    @Test
    public void invokeSendMessageByServiceIfRequested() throws TimeoutException {
        // Create the service Intent


      //  messageRemoteDataSource.sendMessage(dummyHooluMessage,sendMessageCallback);
       // Mockito.verify(binderService).sendMessage(dummyHooluMessage,sendMessageCallback);


    }

    */







}