package com.creativityinmotion.hoolu_messenger_app.utilities;



/**
 * Created by root on 6/24/18.
 */


import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

/**
 * Global executor pools for the whole application.
 * <p>
 * Grouping tasks like this avoids the effects of task starvation (e.g. disk reads don't wait behind
 * webservice requests).
 */
public class DateConverter {




    //Convert Date to Calendar
    public static GregorianCalendar dateToCalendar(Date date) {

        GregorianCalendar calendar = new GregorianCalendar();
        calendar.setTime(date);
        return calendar;

    }

    //Convert Calendar to Date
    public static Date calendarToDate(GregorianCalendar calendar) {
        return calendar.getTime();
    }




    public static long calculateDifferenceInDates(Date firstDate, Date secondDate){

        long diffDays;

        //in milliseconds
        long diff = firstDate.getTime() - secondDate.getTime();

        long diffSeconds = diff / 1000 % 60;
        long diffMinutes = diff / (60 * 1000) % 60;
        long diffHours = diff / (60 * 60 * 1000) % 24;

        diffDays = diff / (24 * 60 * 60 * 1000);

        return diffDays;


    }


    static  String dateString=null;
    public static String dateToString(Date date) {

        //  SimpleDateFormat dateFormatter = new SimpleDateFormat("EEEE, MMMM d, yyyy", Locale.US);
        SimpleDateFormat dateFormatter = new SimpleDateFormat("HH:mm a", Locale.US);


        dateString = dateFormatter.format(date);

        return dateString;
    }


    static String justDateNoTime = null;

    public static String dateToStringWithoutTime(Date date) {


        //  SimpleDateFormat dateFormatter = new SimpleDateFormat("EEEE, MMMM d, yyyy", Locale.US);
        SimpleDateFormat dateFormatter = new SimpleDateFormat("HH:mm:ss SSS a", Locale.US);//


        justDateNoTime = dateFormatter.format(date);

        return justDateNoTime;
    }



    public static Date dateWithTimeToDateWithoutTime(Date date) {


        return stringToDateWithoutTime(dateToStringWithoutTime(date));
    }



    public static Date stringToDateWithoutTime(String date) {
        Date date1 = null;

        SimpleDateFormat dateFormatter = new SimpleDateFormat("MMMM d, yyyy", Locale.US);
        try {

            date1 = dateFormatter.parse(date);

        } catch (ParseException e) {


        }

        return date1;
    }








}
