package com.creativityinmotion.hoolu_messenger_app.utilities;

import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;

import java.util.concurrent.Executor;

public class MainThreadExecutor implements Executor {

    long time;

    public MainThreadExecutor(long time) {
        this.time = time;
    }

    private Handler mainThreadHandler = new Handler(Looper.getMainLooper());


    @Override
    public void execute(@NonNull Runnable runnable) {

        mainThreadHandler.postDelayed(runnable, time);
    }
}