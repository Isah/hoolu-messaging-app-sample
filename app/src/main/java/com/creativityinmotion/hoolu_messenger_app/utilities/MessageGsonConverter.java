package com.creativityinmotion.hoolu_messenger_app.utilities;



/**
 * Created by root on 6/24/18.
 */


import android.arch.persistence.room.TypeConverter;


import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.uzaal.chat.usecases.entities.HooluMessage;

import java.lang.reflect.Type;

/**
 * Global executor pools for the whole application.
 * <p>
 * Grouping tasks like this avoids the effects of task starvation (e.g. disk reads don't wait behind
 * webservice requests).
 */
public class MessageGsonConverter {
    @TypeConverter

    public static HooluMessage fromStringToHooluMessage(String value) {

        Type listType = new TypeToken<HooluMessage>() {}.getType();

        return new Gson().fromJson(value, listType);

    }

    @TypeConverter

    public static String fromHooluMessageToString(HooluMessage hooluMessage) {

        Gson gson = new Gson();

        String json = gson.toJson(hooluMessage);

        return json;

    }


}
