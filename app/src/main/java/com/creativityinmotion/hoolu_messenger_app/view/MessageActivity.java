package com.creativityinmotion.hoolu_messenger_app.view;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.Transformations;
import android.arch.lifecycle.ViewModelProvider;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.AppCompatDelegate;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.OvershootInterpolator;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import com.creativityinmotion.hoolu_messenger_app.R;
import com.creativityinmotion.hoolu_messenger_app.databinding.ActivityMessageListBinding;
import com.creativityinmotion.hoolu_messenger_app.injection.MyApplication;
import com.creativityinmotion.hoolu_messenger_app.viewmodel.MessageActivityViewModel;
import com.creativityinmotion.hoolu_messenger_app.viewmodel.MessageActivityViewModelFactory;
import com.uzaal.chat.usecases.entities.HooluMessage;


import java.util.List;

import javax.inject.Inject;

import jp.wasabeef.recyclerview.adapters.ScaleInAnimationAdapter;
import jp.wasabeef.recyclerview.animators.ScaleInAnimator;
import jp.wasabeef.recyclerview.animators.SlideInUpAnimator;

/**
 * Created by root on 6/26/18.
 */

public class MessageActivity extends AppCompatActivity{

    MessageActivityViewModel messageActivityViewModel;


    @Inject
    MessageActivityViewModelFactory messageActivityViewModelFactory;

    ActivityMessageListBinding messageListBinding;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // look at the fields with annotation , look into the component
        // find the object of the types create and set them to the fields

        MyApplication.get(this).getApplicationComponent().inject(this);


        messageListBinding = DataBindingUtil.setContentView(this,R.layout.activity_message_list);
        //messageListBinding = DataBindingUtil.inflate(inflater, R.layout.activity_message_list, container, false);

        //messageActivityViewModelFactory = new MessageActivityViewModelFactory(Injection.provideSendMessageUseCase(getActivity().getApplication()), Injection.provideGetMessageUseCase(getActivity().getApplication()),PreferenceHandler.getInstance(getContext()));
        setViewModelProvider(getMyViewModelProvider(this, messageActivityViewModelFactory));
        messageActivityViewModel =  viewModelProvider.get(MessageActivityViewModel.class);


        messageTextChanged();
        observeMessageError();
        observeRegistration();
        observeLogin();
        sendMessage();
        setUpRecyclerView();
        setupDrawables();
        setUpToolBar();


        //I'll run a Alarm service to run login even when app is not loaded

    }






    private void setUpToolBar(){

        Toolbar toolbar = messageListBinding.toolbarMain;
        toolbar.setTitle("John");
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {
               //onBackPressed();
               finish();

            }
        });
        toolbar.setLogo(getResources().getDrawable(R.drawable.ic_android_black_24dp));
        toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_arrow_back_black_24dp));
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
         // ((AppCompatActivity)getActivity()).getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_menu_black_24dp);
         // ((AppCompatActivity
         // ((getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);


    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_chat, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void setupDrawables(){

        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
       // messageListBinding.etEnterMessage.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.ic_mood_black_24dp),null,null,null);
        messageListBinding.etEnterMessage.setCompoundDrawablesWithIntrinsicBounds(null,null,getResources().getDrawable(R.drawable.ic_attachment_black_24dp),null);
        //messageListBinding.etEnterMessage.setCompoundDrawablesWithIntrinsicBounds(null,null,getResources().getDrawable(R.drawable.ic_photo_camera_black_24dp),null);

    }




    private ViewModelProvider viewModelProvider = null;


    @VisibleForTesting(otherwise = VisibleForTesting.PROTECTED)
    public void setViewModelProvider(ViewModelProvider viewModelProvider) {
        this.viewModelProvider = viewModelProvider;
    }


    //we are injecting the intance of message factory

    private ViewModelProvider getMyViewModelProvider(MessageActivity  fragment, MessageActivityViewModelFactory modelFactory) {
        if (viewModelProvider == null) {
            viewModelProvider = ViewModelProviders.of(fragment,modelFactory);
        }
        return viewModelProvider;
    }


    private void sendMessage(){
        messageListBinding.btnSendMessage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(messageActivityViewModel.getmMessage() != null) {
                    //messageActivityViewModel.sendmMessage("isah@localhost");
                    messageListBinding.etEnterMessage.setText("");
                    //messageActivityViewModel.register();
                    //messageActivityViewModel.login("petr","petr");
                    messageActivityViewModel.register("brazil","brazil");

                    // Check if no view has focus:
                    View myview = getCurrentFocus();
                    if (myview != null) {
                        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                        if(imm!=null)
                        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                    }

                }
            }
        });

    }



    private void setUpRecyclerView(){
        SlideInUpAnimator animator = new SlideInUpAnimator(new OvershootInterpolator(1f));

        ScaleInAnimator landingAnimator = new ScaleInAnimator();
        messageListBinding.recyclerViewMessages.setItemAnimator(landingAnimator);
        messageListBinding.recyclerViewMessages.getItemAnimator().setAddDuration(3000);

        messageListBinding.recyclerViewMessages.setHasFixedSize(true); //enhance recycler view scroll
        messageListBinding.recyclerViewMessages.setNestedScrollingEnabled(false);// enable smooth scrooll

        messageListBinding.recyclerViewMessages.setLayoutManager(new LinearLayoutManager(getApplicationContext()));

    }

    MessageListAdapter messageListAdapter;
    private void viewMessages(){

        Observer<List<HooluMessage>> houluMessagesObserver = new Observer<List<HooluMessage>>() {
            @Override
            public void onChanged(@Nullable List<HooluMessage> hooluMessages) {

                messageListAdapter = new MessageListAdapter(getApplicationContext(), hooluMessages);
                ScaleInAnimationAdapter alphaAdapter = new ScaleInAnimationAdapter(messageListAdapter);
                alphaAdapter.setFirstOnly(false);
                alphaAdapter.setDuration(1000);
                messageListBinding.recyclerViewMessages.setAdapter(messageListAdapter);
                messageListAdapter.notifyDataSetChanged();
            }
        };

        messageActivityViewModel.getMessageList().observe(this,houluMessagesObserver);

    }

    private void messageTextChanged(){

        messageListBinding.etEnterMessage.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                if(charSequence.length() > 1){
                    messageActivityViewModel.setmMessage(charSequence);

                }

            }

            @Override
            public void afterTextChanged(Editable editable) {

               // editable.clear();


            }
        });
    }

    private void observeMessageError(){

        Observer<String> messageErrorObserver = new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {

                Toast.makeText(getApplicationContext(),""+s,Toast.LENGTH_LONG).show();
            }
        };

       messageActivityViewModel.getmError().observe(this,messageErrorObserver);

    }

    private void observeLogin(){

        Observer<Boolean> messageErrorObserver = new Observer<Boolean>() {
            @Override
            public void onChanged(@Nullable Boolean isLogin) {

                if(isLogin != null && isLogin){

                  viewMessages();

                }else{

                }

            }
        };

        messageActivityViewModel.getmIsLogin().observe(this,messageErrorObserver);

    }

    private void observeRegistration(){

        Observer<String> messageErrorObserver = new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {

                Toast.makeText(getApplicationContext(),"response: "+s,Toast.LENGTH_LONG).show();
            }
        };

        messageActivityViewModel.getmRegisterResponse().observe(this,messageErrorObserver);

    }




}
