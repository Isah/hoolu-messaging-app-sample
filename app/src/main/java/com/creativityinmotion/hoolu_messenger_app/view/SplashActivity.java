package com.creativityinmotion.hoolu_messenger_app.view;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.creativityinmotion.hoolu_messenger_app.utilities.MainThreadExecutor;

public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        Runnable runnable = new Runnable() {
            @Override
            public void run() {


                    Intent intent = new Intent(getApplicationContext(), RegisterActivity.class);
                    startActivity(intent);
                    //  Intent intent = new Intent(this, ActivityLauncher.class);
                    // startActivity(intent);
                    finish();

            }
        };
        new MainThreadExecutor(300).execute(runnable);


    }
}
