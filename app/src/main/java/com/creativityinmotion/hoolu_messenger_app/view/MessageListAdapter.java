package com.creativityinmotion.hoolu_messenger_app.view;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.creativityinmotion.hoolu_messenger_app.R;

import com.creativityinmotion.hoolu_messenger_app.utilities.DateConverter;
import com.uzaal.chat.usecases.entities.HooluMessage;
import com.uzaal.chat.usecases.entities.OperationStatus;

import java.util.List;

/**
 * Created by root on 6/26/18.
 */

public class MessageListAdapter extends RecyclerView.Adapter {
    private Context mContext;
    private List<HooluMessage> mMessageList;


    private static final int VIEW_TYPE_MESSAGE_SENT = 1;
    private static final int VIEW_TYPE_MESSAGE_RECEIVED = 2;


    public MessageListAdapter(Context context, List<HooluMessage> messageList) {
        mContext = context;
        mMessageList = messageList;
    }


    public void updateMessage(HooluMessage hooluMessage){
        mMessageList.add(hooluMessage);
        //mMessageList.
    }


    @Override
    public int getItemCount() {
        return mMessageList.size();
    }
    // Determines the appropriate ViewType according to the sender of the message.
    @Override
    public int getItemViewType(int position) {
        HooluMessage message = mMessageList.get(position);

        if (message.getDeliveryStatus() == HooluMessage.DeliveryStatus.TO) {
            // If the current user is the
            // sender of the message
            return VIEW_TYPE_MESSAGE_SENT;

        } else {
            // If some other user sent the message
            return VIEW_TYPE_MESSAGE_RECEIVED;
        }
    }









    // Inflates the appropriate layout according to the ViewType.
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;

        if (viewType == VIEW_TYPE_MESSAGE_SENT) {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_message_sent, parent, false);

            return new SentMessageHolder(view);

        } else if (viewType == VIEW_TYPE_MESSAGE_RECEIVED) {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_message_received, parent, false);
            return new ReceivedMessageHolder(view);
        }

        return null;
    }

    // Passes the message object to a ViewHolder so that the contents can be bound to UI.
    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        HooluMessage message = mMessageList.get(position);

        switch (holder.getItemViewType()) {
            case VIEW_TYPE_MESSAGE_SENT:
                ((SentMessageHolder) holder).bind(message);
                break;
            case VIEW_TYPE_MESSAGE_RECEIVED:
                ((ReceivedMessageHolder) holder).bind(message);
        }
    }






    private class SentMessageHolder extends RecyclerView.ViewHolder {
        TextView messageText, timeText;
        ImageView messageTick;

        SentMessageHolder(View itemView) {
            super(itemView);

            messageText =  itemView.findViewById(R.id.text_message_body);
            timeText =  itemView.findViewById(R.id.text_message_time);
            messageTick = itemView.findViewById(R.id.ivSent);
        }

        void bind(HooluMessage message) {
            messageText.setText(message.getMessage());
            // Format the stored timestamp into a readable String using method.
            timeText.setText(DateConverter.dateToString(message.getTimeStamp()));

            if(message.getOperationStatus() == OperationStatus.SENT){
             messageTick.setVisibility(View.VISIBLE);
            }else {
             messageTick.setVisibility(View.INVISIBLE);
            }


        }
    }



    //2. Our Adapter needs 2 ViewHolders

    private class ReceivedMessageHolder extends RecyclerView.ViewHolder {
        TextView messageText, timeText, nameText;
        ImageView profileImage;

        ReceivedMessageHolder(View itemView) {
            super(itemView);
            messageText =  itemView.findViewById(R.id.text_message_body);
            timeText = itemView.findViewById(R.id.text_message_time);
            nameText = itemView.findViewById(R.id.text_message_name);
            profileImage =  itemView.findViewById(R.id.image_message_profile);
        }

        void bind(HooluMessage message) {
            messageText.setText(message.getMessage());
            // Format the stored timestamp into a readable String using method.
            timeText.setText( DateConverter.dateToString(message.getTimeStamp()));
            nameText.setText(message.getFrom());

            // Insert the profile image from the URL into the ImageView.
           // Utils.displayRoundImageFromUrl(mContext, message.getSender().getProfileUrl(), profileImage);
        }
    }




}