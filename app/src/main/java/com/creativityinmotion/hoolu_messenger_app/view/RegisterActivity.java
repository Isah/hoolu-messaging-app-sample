package com.creativityinmotion.hoolu_messenger_app.view;

import android.app.ProgressDialog;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.creativityinmotion.hoolu_messenger_app.MainActivity;
import com.creativityinmotion.hoolu_messenger_app.R;
import com.creativityinmotion.hoolu_messenger_app.databinding.ActivityRegisterBinding;

public class RegisterActivity extends AppCompatActivity {


    private static final String TAG = RegisterActivity.class.getSimpleName();
    private ProgressDialog pDialog;
    ActivityRegisterBinding activityRegisterBinnding;


    SessionManager session;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        activityRegisterBinnding = DataBindingUtil.setContentView(this,R.layout.activity_register);

        // Progress dialog
        pDialog = new ProgressDialog(this);
        pDialog.setCancelable(false);

        // Session manager
        session = new SessionManager(getApplicationContext());

        // Check if user is already logged in or not
        if (session.isLoggedIn()) {
            // User is already logged in. Take him to main activity
            Intent intent = new Intent(RegisterActivity.this,
                    MainActivity.class);
            startActivity(intent);
            finish();
        }




    }





    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }






}
