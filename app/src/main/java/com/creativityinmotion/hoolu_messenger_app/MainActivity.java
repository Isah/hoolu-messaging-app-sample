package com.creativityinmotion.hoolu_messenger_app;

import android.content.Intent;
import android.os.Bundle;

import android.support.v7.app.AppCompatActivity;

import android.view.Menu;
import android.view.MenuItem;


import com.creativityinmotion.hoolu_messenger_app.view.MessageActivity;


public class MainActivity extends AppCompatActivity {



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //Toolbar toolbar =  findViewById(R.id.toolbar);
       // setSupportActionBar(toolbar);


        /*
        FragmentManager patientsFragmentMgr = getSupportFragmentManager();
        patientsFragmentMgr.beginTransaction()
                .replace(R.id.fragment_holder, new MessageFragment())
                .addToBackStack("chat")
                .commit();
                */

        Intent intent = new Intent(this, MessageActivity.class);
        startActivity(intent);




    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
