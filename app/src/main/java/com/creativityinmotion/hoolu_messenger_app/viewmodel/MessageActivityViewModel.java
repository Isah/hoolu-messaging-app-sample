package com.creativityinmotion.hoolu_messenger_app.viewmodel;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;
import android.support.annotation.VisibleForTesting;
import android.util.Log;


import com.uzaal.chat.usecases.UseCase;
import com.uzaal.chat.usecases.chat.ChatRepository_Factory;
import com.uzaal.chat.usecases.chat.GetMessages;
import com.uzaal.chat.usecases.chat.ModuleChatUseCase_ProvideChatRepositoryFactory;
import com.uzaal.chat.usecases.chat.SendMessage;
import com.uzaal.chat.usecases.config.PreferenceHandler;
import com.uzaal.chat.usecases.entities.HooluMessage;
import com.uzaal.chat.usecases.entities.OperationStatus;
import com.uzaal.chat.usecases.entities.User;
import com.uzaal.chat.usecases.login.Login;
import com.uzaal.chat.usecases.register.RegisterUser;


import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by root on 6/22/18.
 */

public class MessageActivityViewModel extends ViewModel{


    private SendMessage sendMessage;
    private GetMessages getMessages;
    private RegisterUser registerUser;
    private Login login;
    private PreferenceHandler preferenceHandler;


    public MessageActivityViewModel(SendMessage sendMessage, GetMessages getMessages, RegisterUser registerUser, Login login, PreferenceHandler preferenceHandler) {

        this.sendMessage = sendMessage;
        this.getMessages = getMessages;
        this.registerUser = registerUser;
        this.login = login;
        this.preferenceHandler = preferenceHandler;


    }

    //specify what the visibility sjould have been if not for testin
    @VisibleForTesting(otherwise = VisibleForTesting.PROTECTED)
    MutableLiveData<CharSequence> mMessage;

    public void setmMessage(CharSequence message){
        if(mMessage==null){

            mMessage = new MutableLiveData<>();
        }
     mMessage.postValue(message);
    }

    public MutableLiveData<CharSequence> getmMessage() {
        return mMessage;
    }

    //specify what the visibility sjould have been if not for testin
    @VisibleForTesting(otherwise = VisibleForTesting.PROTECTED)
    MutableLiveData<String> mError;

    public MutableLiveData<String> getmError() {
        if(mError == null){
            mError = new MutableLiveData<>();
        }
        return mError;
    }

    public void setmError(String error) {

        mError.postValue(error);
    }






    private void setUserPreferences(User user){
        user.setUsername(user.getUsername());
        user.setEmail(user.getEmail());
        user.setPassword(user.getPassword());
        preferenceHandler.setUserPrefence(user);

    }




    //specify what the visibility sjould have been if not for testin
    @VisibleForTesting(otherwise = VisibleForTesting.PROTECTED)
    MutableLiveData<List<HooluMessage>> mMessageList;


    private HooluMessage getHooluMessage(List<HooluMessage> messages, String msg_id){

        HooluMessage hooluMessage=null;
        for(HooluMessage message: messages){
            if(message.getMessageId().equals(msg_id)){
              hooluMessage = message;
            }
        }
        return hooluMessage;

    }


    private void addMessageConversion(HooluMessage msg) {

        List<HooluMessage> hooluMessages;

        if(mMessageList.getValue() != null){
            // we have some messages in the list


            Log.d("Inside","msg list");

             //so we need to get the existing list and add to it
          hooluMessages = mMessageList.getValue();


              if(msg.getOperationStatus() == OperationStatus.SENT) {
                  //get the message to remove it coz its old to add a tick
                  HooluMessage hMsg = getHooluMessage(hooluMessages,msg.getMessageId());
                  if(hMsg!=null){//we have the old message
                      hooluMessages.remove(hMsg);
                      hooluMessages.add(msg);
                  }

              }else if(msg.getOperationStatus() == OperationStatus.RECEIVED){
                  HooluMessage hMsg = getHooluMessage(hooluMessages,msg.getMessageId());
                  if(hMsg==null){
                      hooluMessages.add(msg);
                  }

              }
              else  {
                  hooluMessages.add(msg);
              }

          mMessageList.postValue(hooluMessages);


        }else {

            hooluMessages = new ArrayList<>();

            hooluMessages.add(msg);

            mMessageList.postValue(hooluMessages);


        }



    }




    MutableLiveData<String> mRegisterResponse;

    public MutableLiveData<String> getmRegisterResponse() {
        if(mRegisterResponse == null){
            mRegisterResponse = new MutableLiveData<>();
        }
        return mRegisterResponse;
    }

    public void setmRegisterResponse(String response) {

        mRegisterResponse.postValue(response);
    }




    private MutableLiveData<Boolean> mIsLogin;

    public LiveData<Boolean> getmIsLogin() {
        if(mIsLogin == null){
            mIsLogin = new MutableLiveData<>();
        }
        return mIsLogin;
    }
    public void setmIsLogin(boolean isLogin) {

        mIsLogin.postValue(isLogin);
    }



    public void login(String userName, String password){

        Login.RequestValues requestValues = new Login.RequestValues(userName,password);

        login.execute(requestValues, new UseCase.OutputBoundary<Login.ResponseValue>() {
            @Override
            public void onSuccess(Login.ResponseValue result) {

                setmIsLogin(true);
                setmRegisterResponse(result.getConnectionMsg());


            }

            @Override
            public void onNetworkUnAvailable() {
                setmIsLogin(false);
                setmRegisterResponse("failed");

            }

            @Override
            public void onInvalidMessage() {

            }
        });





    }


    public void register(String userName, String password){

        Log.i("RegisterUserVM","started");

        RegisterUser.RequestValues requestValues = new RegisterUser.RequestValues(userName,password);

        registerUser.execute(requestValues, new UseCase.OutputBoundary<RegisterUser.ResponseValue>() {
            @Override
            public void onSuccess(RegisterUser.ResponseValue result) {

                User user = result.getUser();
                setUserPreferences(user);

                String response = user.getUsername()+" registered!";
                setmIsLogin(true);
                setmRegisterResponse(response);

            }

            @Override
            public void onNetworkUnAvailable() {
                String response = "no connection";
                setmIsLogin(false);
                setmRegisterResponse(response);
            }

            @Override
            public void onInvalidMessage() {
                String response = "failed registration";
                setmRegisterResponse(response);

            }
        });

    }




    //1. Viewmodel send message
    public void sendmMessage(String send_to){

        final HooluMessage hooluMessage = new HooluMessage();
        if(mMessage.getValue()!=null)
        hooluMessage.setMessage(mMessage.getValue().toString());
        hooluMessage.setDeliveryStatus(HooluMessage.DeliveryStatus.TO);
        hooluMessage.setTimeStamp(new Date());
        hooluMessage.setTo("admin@localhost");
        hooluMessage.setOperationStatus(OperationStatus.PENDING);

        addMessageConversion(hooluMessage);



        //1. put the message in a RequestModel
        SendMessage.RequestValues requestModel = new SendMessage.RequestValues(hooluMessage);


        //2. execute the usecase
        sendMessage.execute(requestModel, new UseCase.OutputBoundary<SendMessage.ResponseValue>() {
            @Override
            public void onSuccess(SendMessage.ResponseValue result) {

                setIsMessageSent(true);
                hooluMessage.setOperationStatus(OperationStatus.SENT);
                addMessageConversion(hooluMessage);

            }

            @Override
            public void onNetworkUnAvailable() {

                setIsMessageSent(false);

            }

            @Override
            public void onInvalidMessage() {

                setmError("Invalid Message");
            }
        });



      //  getApplication().startService(intent);


    }

    //specify what the visibility should have been if not for testin
    @VisibleForTesting(otherwise = VisibleForTesting.PROTECTED)
    MutableLiveData<Boolean> isMessageSent;

    public MutableLiveData<Boolean> getIsMessageSent() {
        return isMessageSent;
    }

    public void setIsMessageSent(boolean is_message_sent) {
        if(isMessageSent == null){
           isMessageSent = new MutableLiveData<>();
        }

        this.isMessageSent.postValue(is_message_sent);
    }



    //3. my observer will observe this guy after a messagee has been sent
    public void readMessages(String userId){

        //get all conversation msgs from the database

        HooluMessage hooluMessageTo = new HooluMessage();
        hooluMessageTo.setMessage("How are you friend !");
        hooluMessageTo.setDeliveryStatus(HooluMessage.DeliveryStatus.TO);
        hooluMessageTo.setFrom("piero");
        hooluMessageTo.setTimeStamp(new Date());


        HooluMessage hooluMessageFrom = new HooluMessage();
        hooluMessageFrom.setMessage("Im fine, How are you !");
        hooluMessageFrom.setDeliveryStatus(HooluMessage.DeliveryStatus.FROM);
        hooluMessageFrom.setFrom("jason");
        hooluMessageFrom.setTimeStamp(new Date());


        ArrayList<HooluMessage> hmsgs = new ArrayList<>();
        hmsgs.add(hooluMessageTo);
        hmsgs.add(hooluMessageFrom);


        mMessageList.postValue(hmsgs);

        final GetMessages.RequestValues requestValues = new GetMessages.RequestValues(userId);


        getMessages.execute(requestValues, new UseCase.OutputBoundary<GetMessages.ResponseValue>() {
            @Override
            public void onSuccess(GetMessages.ResponseValue responseValue) {

                HooluMessage hooluMessage = responseValue.getmHooluMessage();
                hooluMessage.setOperationStatus(OperationStatus.RECEIVED);
                addMessageConversion(hooluMessage);

            }

            @Override
            public void  onNetworkUnAvailable() {

            }



            @Override
            public void onInvalidMessage() {

            }
        });





    }


    public MutableLiveData<List<HooluMessage>> getMessageList(){
        if(mMessageList == null){
            mMessageList = new MutableLiveData<>();
        }

        User user = preferenceHandler.getUserFromPreferences();

        readMessages(user.getUsername());

        return mMessageList;
    }






}
