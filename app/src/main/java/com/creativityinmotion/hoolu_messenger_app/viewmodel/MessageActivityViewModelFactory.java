package com.creativityinmotion.hoolu_messenger_app.viewmodel;

import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider;

import com.uzaal.chat.usecases.chat.GetMessages;
import com.uzaal.chat.usecases.chat.SendMessage;
import com.uzaal.chat.usecases.config.PreferenceHandler;
import com.uzaal.chat.usecases.login.Login;
import com.uzaal.chat.usecases.register.RegisterUser;

import javax.inject.Inject;


public class MessageActivityViewModelFactory implements ViewModelProvider.Factory{


    private final SendMessage sendMessage;
    private final GetMessages getMessages;
    RegisterUser registerUser;
    private final Login login;
    private final PreferenceHandler preferenceHandler;

    @Inject
    public MessageActivityViewModelFactory(SendMessage sendMessage, GetMessages getMessages, RegisterUser registerUser, Login login, PreferenceHandler preferenceHandler) {
        this.sendMessage = sendMessage;
        this.getMessages = getMessages;
        this.registerUser = registerUser;
        this.login = login;
        this.preferenceHandler = preferenceHandler;
    }

    @Override
    public <T extends ViewModel> T create(Class<T> modelClass) {
        if(modelClass.isAssignableFrom(MessageActivityViewModel.class)){

            return (T) new MessageActivityViewModel(sendMessage,getMessages,registerUser,login,preferenceHandler);

        }
        else{

            throw new IllegalArgumentException("View Model not found");
        }

    }




}