package com.creativityinmotion.hoolu_messenger_app.injection;

import android.content.Context;

import com.creativityinmotion.hoolu_messenger_app.view.MessageActivity;
import com.creativityinmotion.hoolu_messenger_app.viewmodel.MessageActivityViewModelFactory;
import com.uzaal.chat.usecases.chat.ModuleChatUseCase;
import com.uzaal.chat.usecases.config.PreferenceHandler;
import com.uzaal.chat.usecases.login.ModuleLoginUseCase;
import com.uzaal.chat.usecases.register.ModuleRegisterUseCase;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by root on 6/29/18.
 * Acts as the Interface for your entire dependency graph
 *
 * educate our component of the modules
 */

@Singleton
@Component(modules = {ModuleChatUseCase.class,ModuleLoginUseCase.class, ModuleRegisterUseCase.class,  ModuleAndroid.class})
public interface ApplicationComponent {

    Context context();

    //inject my view model factory into the message activity
    void inject(MessageActivity messageFragment);

    //inject use cases into view model factory
    void inject(MessageActivityViewModelFactory messageActivityViewModelFactory);


    //inject application into the preference handler
    void inject(PreferenceHandler preferenceHandler);




}
