package com.creativityinmotion.hoolu_messenger_app.injection;

import android.app.Application;
import android.content.Context;

import com.creativityinmotion.hoolu_messenger_app.viewmodel.MessageActivityViewModelFactory;
import com.uzaal.chat.usecases.chat.GetMessages;
import com.uzaal.chat.usecases.chat.SendMessage;
import com.uzaal.chat.usecases.config.PreferenceHandler;
import com.uzaal.chat.usecases.login.Login;
import com.uzaal.chat.usecases.register.RegisterUser;
import com.uzaal.chat.usecases.utilities.AppExecutors;
import javax.inject.Singleton;
import dagger.Module;
import dagger.Provides;

@Module
public class ModuleAndroid {


    @Provides
    @Singleton
    public MessageActivityViewModelFactory provideMesssageViewModelFactory(SendMessage sendMessage, GetMessages getMessages, RegisterUser registerUser, Login login, PreferenceHandler preferenceHandler){

        return new MessageActivityViewModelFactory(sendMessage,getMessages,registerUser,login,preferenceHandler);

    }


    @Provides
    public AppExecutors provideAppExecutors(){

        return  new AppExecutors();
    }

    @Provides
    @Singleton
    public PreferenceHandler providePreferenceHandler(Application application){

        return new PreferenceHandler(application);
    }



    private Context context;
    private Application application;

    public ModuleAndroid(Context context, Application application){
        this.context = context;
        this.application = application;
    }

    @Provides
    public Context  provideContext(){
        return context;
    }

    @Provides
    public Application  provideApplicationContext(){
        return application;
    }






}
