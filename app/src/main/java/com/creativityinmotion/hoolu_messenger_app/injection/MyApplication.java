package com.creativityinmotion.hoolu_messenger_app.injection;

import android.app.Application;
import android.content.Context;

import com.uzaal.chat.usecases.chat.ModuleChatUseCase;
import com.uzaal.chat.usecases.login.ModuleLoginUseCase;
import com.uzaal.chat.usecases.register.ModuleRegisterUseCase;

/**
 * Created by root on 6/29/18.
 */

public class MyApplication extends Application{

    private ApplicationComponent applicationComponent;

    public static MyApplication get(Context context){
        return ((MyApplication) context.getApplicationContext());

    }


    @Override
    public void onCreate() {
        super.onCreate();

        applicationComponent = DaggerApplicationComponent.builder()
                .moduleChatUseCase(new ModuleChatUseCase())
                .moduleLoginUseCase(new ModuleLoginUseCase())
                .moduleRegisterUseCase(new ModuleRegisterUseCase())
                .moduleAndroid(new ModuleAndroid(this,this))
                .build();

    }


    public ApplicationComponent getApplicationComponent() {
        return applicationComponent;
    }
}
