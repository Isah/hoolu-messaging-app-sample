package com.uzaal.chat.usecases.login;

import android.content.Context;
import android.support.annotation.NonNull;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by root on 6/29/18.
 */


@Module
public class ModuleLoginUseCase {

      @Provides
      public Login provideLoginUseCase(@NonNull LoginRepository messsageRepository) {

         return new Login(messsageRepository);

      }


    @Provides
    @Singleton
    public LoginRepository provideLoginRepository(LoginServiceClient chatServiceClient){

        return new LoginRepository(chatServiceClient);

    }


    @Provides
    @Singleton
    public LoginServiceClient provideSmackClient(Context context){

        return new LoginSmackServiceClient(context);

    }




}
