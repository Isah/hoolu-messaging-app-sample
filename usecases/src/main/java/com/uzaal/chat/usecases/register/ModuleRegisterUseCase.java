package com.uzaal.chat.usecases.register;

import android.content.Context;
import android.support.annotation.NonNull;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by root on 6/29/18.
 */


@Module
public class ModuleRegisterUseCase {

      @Provides
      public RegisterUser provideLoginUseCase(@NonNull RegisterRepository messsageRepository) {

         return new RegisterUser(messsageRepository);

      }

    @Provides
    @Singleton
    public RegisterRepository provideLoginRepository(RegisterServiceClient chatServiceClient){

        return new RegisterRepository(chatServiceClient);

    }


    @Provides
    @Singleton
    public RegisterServiceClient provideSmackRegisterClient(Context context){

        return new RegisterSmackServiceClient(context);

    }





}
