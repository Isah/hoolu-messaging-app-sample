package com.uzaal.chat.usecases.login;


import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.util.Log;
import com.uzaal.chat.usecases.config.HostConfig;

import javax.inject.Inject;

/**
 * Created by root on 7/3/18.
 */

class LoginSmackServiceClient implements LoginSmackService.OnConnectionEstablishedListener, LoginServiceClient{


    /***
     * Job: Execute the  BinderService tasks
     * 1. start the service
     * 2. invoke login, send msg, get msg
     * 3. all those invocations happen after a connection has been established
     *    that's why we have the connection listener
     */



    Context myContext;

    //my intent service needs Myxmpp
    private LoginSmackService binderService;
    boolean mBound = false;


    // Prevent direct instantiation.
    @Inject
    public LoginSmackServiceClient(Context myContext) {
        this.myContext = myContext;

    }


    public void bindtoLocalService(String userName, String pass, LogInCallback logInCallback){

         // myContext =  preferenceHandler.getActivityContext();
         // Bind to LocalService
         Intent intent = new Intent(myContext, LoginSmackService.class);
         //User user = HostConfig.getUser(myContext);

         intent.putExtra("usr",userName);
         intent.putExtra("pwd",pass);
         intent.putExtra("host", HostConfig.HOST);
         intent.putExtra("host_address",HostConfig.HOST_ADDRESS);

         Log.d("BoundServiceBefore", ""+myContext.getPackageName());


        myContext.bindService(intent, new ServiceConnection() {
            @Override
            public void onServiceConnected(ComponentName componentName, IBinder iBinder) {

                //LocalBinder binder = (LocalBinder) iBinder;
                binderService = ((LoginSmackService.LocalBinder) iBinder).getService();

                Log.d("Bound Service Conn", ""+ binderService.started);

                //binderService = (MyXMPPBinderService) binder.getService();
                if(binderService != null){
                    Log.d("service-bind", "Service is bonded successfully!");
                    mBound = true;
                    binderService.setOnConnectionEstablishedListener(LoginSmackServiceClient.this);
                    //do whatever you want to do after successful binding


                    binderService.connectLogin(logInCallback);

                }else {

                    Log.d("service-bind", "Service is not bonded !");

                }


            }

            @Override
            public void onServiceDisconnected(ComponentName componentName) {
                Log.d("ServiceDisconnected", ""+myContext.getPackageName());

            }
        }, Context.BIND_AUTO_CREATE);

        Log.d("Bound Service After", ""+myContext.getPackageName());


    }

    private boolean isConnectionEstablished = false;
    protected boolean isXmppConnectionEstablished = false;


    @Override
    public void onConnectionEstablished() {

        // At this point the service has been bound and connected to the server
        // Do stuff here
        // Note: This method is called from a non-UI thread.
            isConnectionEstablished = true;

            /*
            binderService.makeConnectionToXmpp(new ChatDataSource.ConnectionCallback() {
                @Override
                public void onConnected() {

                    isXmppConnectionEstablished = true;

                }

                @Override
                public void onConnectionFailed() {

                    isXmppConnectionEstablished = false;


                }
            });
            */
    }



    /** Defines callbacks for service binding, passed to bindService() */

    /**
     * Note: {@link } ()} is never fired. In a real remote data
     * source implementation, this would be fired if the server can't be contacted or the server
     * returns an error.
     */


    @Override
    public void logIn(String userName, String pass, LogInCallback logInCallback) {

        Log.d("ServiceClientLogin","in service login");


        if(!mBound) {
            bindtoLocalService(userName,pass,logInCallback);
        }

    }


}
