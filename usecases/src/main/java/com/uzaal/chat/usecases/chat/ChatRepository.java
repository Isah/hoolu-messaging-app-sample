package com.uzaal.chat.usecases.chat;


import android.support.annotation.VisibleForTesting;

import com.uzaal.chat.usecases.entities.HooluMessage;
import javax.inject.Inject;
import static dagger.internal.Preconditions.checkNotNull;

/**
 * Created by root on 6/29/18.
 */

@VisibleForTesting(otherwise = VisibleForTesting.PROTECTED)
public class ChatRepository implements ChatDataSource {

    //The client that connects to Android Service sends the messages
    // SmackXmppClient,SomeOtherXmppClient
    private ChatServiceClient chatServiceClient;//there can be different clients (smack)
    private MessageDao messageDAO;//there can be different LocalDataStores (Room,file)

    private static ChatRepository INSTANCE = null;


    @Inject
    public ChatRepository(ChatServiceClient msgService, MessageDao messageDAO){

        checkNotNull(msgService);
        //msgService.bindtoLocalService();

        this.chatServiceClient = msgService;
        this.messageDAO = messageDAO;


    }



    @Override
    public void connect(ConnectionCallback connectionCallback) {



    }


    @Override
    public void saveMessage(HooluMessage hooluMessage, SaveMessagesCallback saveMessagesCallback) {

    }

    @Override
    public void sendMessage(HooluMessage hooluMessage, SendMessageCallback sendMessageCallback) {

        chatServiceClient.sendMessage(hooluMessage, new SendMessageCallback() {
            @Override
            public void onMessageSent() {

                messageDAO.saveMessage(hooluMessage, new SaveMessagesCallback() {
                    @Override
                    public void onMessageSaved(String messages) {

                        sendMessageCallback.onMessageSent();
                    }

                    @Override
                    public void onSaveFailed() {

                        sendMessageCallback.onMessageFailed();

                    }
                });


            }

            @Override
            public void onMessageFailed() {

                sendMessageCallback.onMessageFailed();

            }
        });



    }

    @Override
    public void getMessage(String userId, GetMessageCallback getMessageCallback) {

        chatServiceClient.getMessage(userId,getMessageCallback);

    }

    @Override
    public void getMessages(String userId, GetMessagesCallback getMessageCallback) {

        messageDAO.getMessages(userId,getMessageCallback);

    }
}
