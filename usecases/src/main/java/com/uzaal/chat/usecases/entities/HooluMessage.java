package com.uzaal.chat.usecases.entities;

import org.jivesoftware.smackx.si.packet.StreamInitiation;

import java.io.File;
import java.util.Date;
import java.util.UUID;

/**
 * Created by root on 7/5/18.
 */

public class HooluMessage {

   private String userID;
   private String message;
   private String messageId;
   private File file;
   private DeliveryStatus deliveryStatus;
   private OperationStatus operationStatus;

   private Date timeStamp;
   private String from;
   private String to;


   public HooluMessage() {
      String mid = UUID.randomUUID().toString();
      setMessageId(mid);
   }

   public String getMessageId() {
      return messageId;
   }

   public void setMessageId(String messageId) {
      this.messageId = messageId;
   }

   public OperationStatus getOperationStatus() {
      return operationStatus;
   }

   public void setOperationStatus(OperationStatus operationStatus) {
      this.operationStatus = operationStatus;
   }

   public DeliveryStatus getDeliveryStatus() {
      return deliveryStatus;
   }

   public void setDeliveryStatus(DeliveryStatus deliveryStatus) {
      this.deliveryStatus = deliveryStatus;
   }

   public String getFrom() {
      return from;
   }

   public void setFrom(String from) {
      this.from = from;
   }


   public String getUserID() {

      return userID;
   }

   public void setUserID(String userID) {
      this.userID = userID;
   }

   public String getTo() {
      return to;
   }

   public void setTo(String to) {
      this.to = to;
   }

   public String getMessage() {
      return message;
   }

   public void setMessage(String message) {
      this.message = message;
   }

   public Date getTimeStamp() {
      return timeStamp;
   }

   public void setTimeStamp(Date timeStamp) {
      this.timeStamp = timeStamp;
   }


  public enum DeliveryStatus{
      TO,FROM


  }

}
