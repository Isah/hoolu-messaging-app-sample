package com.uzaal.chat.usecases.chat;


import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.util.Log;


import com.uzaal.chat.usecases.config.HostConfig;
import com.uzaal.chat.usecases.config.PreferenceHandler;
import com.uzaal.chat.usecases.entities.HooluMessage;
import com.uzaal.chat.usecases.entities.User;

import javax.inject.Inject;

import static com.google.gson.internal.$Gson$Preconditions.checkNotNull;

/**
 * Created by root on 7/3/18.
 */

class ChatSmackServiceClient implements ChatSmackService.OnConnectionEstablishedListener, ChatServiceClient{


    /***
     * Job: Execute the  BinderService tasks
     * 1. start the service
     * 2. invoke login, send msg, get msg
     * 3. all those invocations happen after a connection has been established
     *    that's why we have the connection listener
     */



    Context myContext;

    //my intent service needs Myxmpp
    private ChatSmackService binderService;
    boolean mBound = false;



    // Prevent direct instantiation.
    @Inject
    public ChatSmackServiceClient(Context myContext, PreferenceHandler preferenceHandler) {
        this.myContext = myContext;
        if(!mBound) {
        bindtoLocalService(preferenceHandler);
        }

    }


    public void bindtoLocalService(PreferenceHandler preferenceHandler){

         // myContext =  preferenceHandler.getActivityContext();
         // Bind to LocalService
         Intent intent = new Intent(myContext, ChatSmackService.class);
         User user = preferenceHandler.getUserFromPreferences();

         intent.putExtra("usr",user.getUsername());
         intent.putExtra("pwd",user.getPassword());
         intent.putExtra("host", HostConfig.HOST);
         intent.putExtra("host_address",HostConfig.HOST_ADDRESS);

         Log.d("Bound Service Before", ""+myContext.getPackageName());

        // application.startService(intent);
        myContext.bindService(intent, new ServiceConnection() {
            @Override
            public void onServiceConnected(ComponentName componentName, IBinder iBinder) {

                //LocalBinder binder = (LocalBinder) iBinder;
                binderService = ((ChatSmackService.LocalBinder) iBinder).getService();

                Log.d("Bound Service Conn", ""+ binderService.started);

                //binderService = (MyXMPPBinderService) binder.getService();
                if(binderService != null){
                    Log.d("service-bind", "Service is bonded successfully!");
                    mBound = true;
                    binderService.setOnConnectionEstablishedListener(ChatSmackServiceClient.this);
                    //do whatever you want to do after successful binding
                }else {

                    Log.d("service-bind", "Service is not bonded !");

                }


            }

            @Override
            public void onServiceDisconnected(ComponentName componentName) {

            }
        }, Context.BIND_AUTO_CREATE);

        Log.d("Bound Service After", ""+myContext.getPackageName());


    }

    private boolean isConnectionEstablished = false;
    protected boolean isXmppConnectionEstablished = false;


    @Override
    public void onConnectionEstablished() {

        // At this point the service has been bound and connected to the server
        // Do stuff here
        // Note: This method is called from a non-UI thread.
            isConnectionEstablished = true;

            /*
            binderService.makeConnectionToXmpp(new ChatDataSource.ConnectionCallback() {
                @Override
                public void onConnected() {

                    isXmppConnectionEstablished = true;

                }

                @Override
                public void onConnectionFailed() {

                    isXmppConnectionEstablished = false;


                }
            });
            */
    }



    /** Defines callbacks for service binding, passed to bindService() */

    /**
     * Note: {@link } ()} is never fired. In a real remote data
     * source implementation, this would be fired if the server can't be contacted or the server
     * returns an error.
     */


    @Override
    public void sendMessage(HooluMessage hooluMessage, ChatDataSource.SendMessageCallback sendMessageCallback) {


        if(isXmppConnectionEstablished) {
            binderService.sendMessage(hooluMessage, sendMessageCallback);
        }

    }

    @Override
    public void getMessage(String userid, ChatDataSource.GetMessageCallback getMessageCallback) {

        //this is where the retrofit or xmpp msg client would come in
        checkNotNull(getMessageCallback);

        if(isXmppConnectionEstablished) {
            binderService.getMessage(userid, getMessageCallback);
        }


    }


}
