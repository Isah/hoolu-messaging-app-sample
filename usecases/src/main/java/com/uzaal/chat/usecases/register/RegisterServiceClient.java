package com.uzaal.chat.usecases.register;


import com.uzaal.chat.usecases.entities.User;

/**
 * Created by root on 7/3/18.
 */

interface RegisterServiceClient {


    interface RegisterCallback{

        void onUserRegistered(User user);
        void onRegistrationFailed();
        void onConnectionFailed();

    }

    void register(String userName, String password, RegisterCallback registerCallback);



}
