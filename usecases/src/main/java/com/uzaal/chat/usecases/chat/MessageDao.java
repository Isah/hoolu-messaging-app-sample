package com.uzaal.chat.usecases.chat;

import com.uzaal.chat.usecases.entities.HooluMessage;

import java.util.List;

interface MessageDao {

    void saveMessage(HooluMessage hooluMessage, ChatDataSource.SaveMessagesCallback saveMessagesCallback);
    List<HooluMessage> getMessages(String userId, ChatDataSource.GetMessagesCallback getMessagesCallback);


}
