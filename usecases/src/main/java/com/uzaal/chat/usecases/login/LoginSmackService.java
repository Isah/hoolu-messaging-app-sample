package com.uzaal.chat.usecases.login;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.Handler;
import android.os.IBinder;
import android.support.annotation.RestrictTo;
import android.util.Log;

import com.uzaal.chat.usecases.config.ConnectionConfig;
import com.uzaal.chat.usecases.utilities.AppExecutors;

import org.jivesoftware.smack.SmackException;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smackx.ping.PingManager;

import java.io.IOException;
import java.util.Date;

import static android.content.ContentValues.TAG;

/**
 * Created by root on 7/3/18.
 */

public class LoginSmackService extends Service {


    private final String MyTAG = "LoginSmack";
    private String userName;
    private String passWord;
    private String message;
    private String from;
    private String to;
    private String HOST;
    private String HOST_ADDRESS;


   // private ChatXmpp myXMPP = ChatXmpp.getInstance();
    private AppExecutors appExecutors = new AppExecutors();


    @RestrictTo (RestrictTo.Scope.TESTS)
    public boolean started = false;

    @RestrictTo (RestrictTo.Scope.TESTS)
    public boolean stopped = false;

    @RestrictTo (RestrictTo.Scope.TESTS)
    public boolean isOnStartCommandStarted = false;


    //1. Binder given to clients to bind to services
    private final IBinder mBinder = new LocalBinder();


    /**2
     * Class used for the client Binder.  Because we know this service always
     * runs in the same process as its clients, we don't need to deal with IPC.
     */
    public class LocalBinder extends Binder {
       public LoginSmackService getService() {
            // Return this instance of LocalService so clients can call public methods
            return LoginSmackService.this;
        }
    }


    // To provide binding for a service i need to implement onBind()
    // returns a IBinder that provides a programming interface that clients
    // can use to interact with the service

    @Override
    public IBinder onBind(Intent intent) {

        if(intent != null){

            passWord = intent.getStringExtra("pwd");
            userName = intent.getStringExtra("usr");
            HOST = intent.getStringExtra("host");
            HOST_ADDRESS = intent.getStringExtra("host_address");

            Log.d("SERVICE CONN BOUND:", "time: "+new Date());

        }
        return mBinder;
    }



    private boolean mIsConnectionEstablished = false;



    public interface OnConnectionEstablishedListener {
        void onConnectionEstablished();
    }
    private OnConnectionEstablishedListener mListener;

    //the one that impl this interface will get the call back
    public void setOnConnectionEstablishedListener(OnConnectionEstablishedListener listener) {

        mListener = listener;

        // Already connected to server. Notify immediately.
        if(mIsConnectionEstablished) {
            mListener.onConnectionEstablished();
        }
    }





    private void notifyConnectionEstablished() {
        mIsConnectionEstablished = true;
        if(mListener != null) {
            mListener.onConnectionEstablished();
        }
    }





    @Override
    public void onCreate() {
        super.onCreate();
        started = true;

        final Handler handler = new Handler();
        Runnable runnable = new Runnable() {
            @Override
            public void run() {

                Log.d("SERVICE CONN EST: ", "time: "+new Date());

                notifyConnectionEstablished();


            }
        };
        handler.postDelayed(runnable,5000);

        appExecutors.diskIO().execute(runnable);


    }

    @Override
    public void onDestroy() {
       // myXMPP.disconnectConnection();
        super.onDestroy();
        stopped = true;
    }

    ConnectionConfig myXMPPConnection = ConnectionConfig.getInstance();


    private boolean login() {

        try {
            Log.d(MyTAG, "Inside XMPP login method!");
            long l = System.currentTimeMillis();


            if (myXMPPConnection.getTcpConnection().isAuthenticated()) {
                Log.d(MyTAG, "User already logged in");
                return  true;
            }

            myXMPPConnection.getTcpConnection().login(userName, passWord);

            Log.d(MyTAG, "Yey! We're connected to the Xmpp server!");

        } catch (XMPPException | SmackException | IOException e) {
            Log.e(TAG, "Issue in login, check the stacktrace"); e.printStackTrace();
        } catch (Exception e) {
        }
        PingManager pingManager = PingManager.getInstanceFor(myXMPPConnection.getTcpConnection());

        pingManager.setPingInterval(5000);


        return myXMPPConnection.getTcpConnection().isAuthenticated();

    }


    public void connectLogin(final LoginServiceClient.LogInCallback logInCallback){

        Runnable runnable = new Runnable() {
            @Override
            public void run() {


                //Looper.prepare();
                //final Handler handler = new Handler();
                Runnable runnable = new Runnable() {
                    @Override
                    public void run() {

                        if(myXMPPConnection.isConnected()){

                            Log.d(MyTAG, "Connected");

                            logInCallback.onUserLogIn();

                        }
                        else {

                            Log.d(MyTAG, "Connected Failed");

                            myXMPPConnection.configureConnection(HOST,HOST_ADDRESS);


                            //wait
                            try {
                                Thread.sleep(5000);
                            }catch (Exception e){ }


                            if(login()){

                                logInCallback.onUserLogIn();
                            }else {

                                logInCallback.onLogInFailed();
                            }


                        }


                    }
                };

                appExecutors.diskIO().execute(runnable);



            }
        };

        appExecutors.networkIO().execute(runnable);


    }










}





