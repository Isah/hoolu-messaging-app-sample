package com.uzaal.chat.usecases.entities;

import java.util.List;

public class User {


    private String mobile;
    private String username;
    private String password;
    private String email;

    //messages stored in the database
    private List<HooluMessage> hooluMessages;



    public List<HooluMessage> getHooluMessages() {
        return hooluMessages;
    }

    public void setHooluMessages(List<HooluMessage> hooluMessages) {
        this.hooluMessages = hooluMessages;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
