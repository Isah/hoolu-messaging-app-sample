package com.uzaal.chat.usecases.login;

import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;
import android.util.Log;

import com.uzaal.chat.usecases.UseCase;
import com.uzaal.chat.usecases.entities.HooluMessage;

import javax.inject.Inject;

/**
 * Created by root on 6/29/18.
 */

public class Login implements UseCase<Login.RequestValues, Login.ResponseValue> {


    private final String MyTAG = "LoginUsease";


    LoginRepository loginRepository;

    @Inject
    public Login(LoginRepository loginRepository){

    this.loginRepository = loginRepository;

    }



    //specify what the visibility sjould have been if not for testin
   @VisibleForTesting(otherwise = VisibleForTesting.PROTECTED)
    public static ResponseValue responseValue;





    @Override
    public void execute(RequestValues requestValues, final OutputBoundary<ResponseValue> deliveryMechanism) {

        //somebody else will implemtnt deliverMech not me so ill just call it
        //but ill implement the sending (new) coz i need the results

        String userName = requestValues.getUserName();
        String password = requestValues.getPassword();

        Log.d(MyTAG,"in execute");


        loginRepository.logIn(userName, password,  new LoginServiceClient.LogInCallback() {
            @Override
            public void onUserLogIn() {

                deliveryMechanism.onSuccess(new ResponseValue(userName));

            }

            @Override
            public void onLogInFailed() {
                deliveryMechanism.onNetworkUnAvailable();

            }
        });


    }





    //Validator usecase
    private boolean isValid(HooluMessage hooluMessage){

        if(hooluMessage.getMessage().isEmpty()){

            return false;

        }
        else if(hooluMessage.getMessage().length() > 30) {

        return false;

        }

        else{

            return true;
        }

    }




    //it implements such that its of a right type to be accepted in thhe execute method
    public static final class RequestValues implements UseCase.RequestValues {

        private final String userName;
        private final String password;

        public RequestValues(@NonNull String userName, String password) {
            this.userName = userName;
            this.password = password;

        }

        public String getUserName() {
            return userName;
        }

        public String getPassword() {
            return password;
        }

    }

    public static final class ResponseValue implements UseCase.ResponseValue {

        private String connectionMsg;

        public ResponseValue(@NonNull String connectionMsg) {
            this.connectionMsg = connectionMsg;
        }

        public String getConnectionMsg() {
            return connectionMsg;
        }



    }





}
