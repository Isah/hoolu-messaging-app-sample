package com.uzaal.chat.usecases.register;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.Handler;
import android.os.IBinder;
import android.support.annotation.RestrictTo;
import android.util.Log;

import com.uzaal.chat.usecases.config.ConnectionConfig;
import com.uzaal.chat.usecases.entities.User;
import com.uzaal.chat.usecases.utilities.AppExecutors;

import org.jivesoftware.smack.SmackException;
import org.jivesoftware.smackx.iqregister.AccountManager;
import org.jxmpp.jid.parts.Localpart;

import java.io.IOException;
import java.util.Date;

import static android.content.ContentValues.TAG;

/**
 * Created by root on 7/3/18.
 */

class RegisterSmackService extends Service {


    private String userName;
    private String passWord;
    private String message;
    private String from;
    private String to;
    private String HOST;
    private String HOST_ADDRESS;


   // private ChatXmpp myXMPP = ChatXmpp.getInstance();
    private AppExecutors appExecutors = new AppExecutors();


    @RestrictTo (RestrictTo.Scope.TESTS)
    public boolean started = false;

    @RestrictTo (RestrictTo.Scope.TESTS)
    public boolean stopped = false;

    @RestrictTo (RestrictTo.Scope.TESTS)
    public boolean isOnStartCommandStarted = false;


    //1. Binder given to clients to bind to services
    private final IBinder mBinder = new LocalBinder();


    /**2
     * Class used for the client Binder.  Because we know this service always
     * runs in the same process as its clients, we don't need to deal with IPC.
     */
    public class LocalBinder extends Binder {
       public RegisterSmackService getService() {
            // Return this instance of LocalService so clients can call public methods
            return RegisterSmackService.this;
        }
    }


    // To provide binding for a service i need to implement onBind()
    // returns a IBinder that provides a programming interface that clients
    // can use to interact with the service

    @Override
    public IBinder onBind(Intent intent) {

        if(intent != null){

            passWord = intent.getStringExtra("pwd");
            userName = intent.getStringExtra("usr");
            HOST = intent.getStringExtra("host");
            HOST_ADDRESS = intent.getStringExtra("host_address");

            Log.d("SERVICE CONN BOUND: ", "time: "+new Date());


        }
        return mBinder;
    }



    private boolean mIsConnectionEstablished = false;



    public interface OnConnectionEstablishedListener {
        void onConnectionEstablished();
    }
    private OnConnectionEstablishedListener mListener;

    //the one that impl this interface will get the call back
    public void setOnConnectionEstablishedListener(OnConnectionEstablishedListener listener) {

        mListener = listener;

        // Already connected to server. Notify immediately.
        if(mIsConnectionEstablished) {
            mListener.onConnectionEstablished();
        }
    }





    private void notifyConnectionEstablished() {
        mIsConnectionEstablished = true;

        if(mListener != null) {
            mListener.onConnectionEstablished();
        }
    }






    @Override
    public void onCreate() {
        super.onCreate();
        started = true;

        final Handler handler = new Handler();
        Runnable runnable = new Runnable() {
            @Override
            public void run() {

                Log.d("SERVICE CONN EST: ", "time: "+new Date());

                notifyConnectionEstablished();


            }
        };
        handler.postDelayed(runnable,5000);

        appExecutors.diskIO().execute(runnable);


    }

    @Override
    public void onDestroy() {
       // myXMPP.disconnectConnection();
        super.onDestroy();
        stopped = true;
    }

    ConnectionConfig myXMPPConnection = ConnectionConfig.getInstance();




    public void  registerUser(String userName, String passWord, RegisterServiceClient.RegisterCallback registerCallback){

        myXMPPConnection.setUserOnConnection(userName,passWord);
        myXMPPConnection.configureConnection(HOST,HOST_ADDRESS);

        final Handler handler = new Handler();
        Runnable runnable = new Runnable() {
            @Override
            public void run() {

             createAccount(userName,passWord,registerCallback);

            }
        };
        handler.postDelayed(runnable,10000);

        appExecutors.diskIO().execute(runnable);


    }




    private boolean createAccount(String user, String pass, RegisterServiceClient.RegisterCallback registerCallback)  {
        //throws XMPPException, SmackException.NoResponseException, SmackException.NotConnectedException

        long l = System.currentTimeMillis();
        try {

            AccountManager accountManager = AccountManager.getInstance(myXMPPConnection.getConnection());
            accountManager.sensitiveOperationOverInsecureConnection(true);
            accountManager.createAccount(Localpart.from(user), pass);

             User user1 = new User();
             user1.setUsername(user);
             user1.setPassword(pass);
             user1.setEmail(Localpart.from(user).toString());
             registerCallback.onUserRegistered(user1);

            return true;


        } catch (SmackException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (Exception e) {
            //PurplKiteXMPPConnectException

        }

        registerCallback.onRegistrationFailed();
        Log.i(TAG, "Time taken to register: " + (System.currentTimeMillis() - l));
        return  false;

    }









}





