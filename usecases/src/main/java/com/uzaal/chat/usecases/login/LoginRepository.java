package com.uzaal.chat.usecases.login;


import android.util.Log;

import javax.inject.Inject;

import static dagger.internal.Preconditions.checkNotNull;

/**
 * Created by root on 6/29/18.
 */

class LoginRepository implements LoginServiceClient {

    //The client that connects to Android Service sends the messages
    // SmackXmppClient,SomeOtherXmppClient
    private LoginServiceClient loginDataSource;//there can be different login srcs (smack)
    private static LoginRepository INSTANCE = null;

    private final String MyTAG = "LoginRepo";


    @Inject
    public LoginRepository(LoginServiceClient loginDataSource){
        checkNotNull(loginDataSource);
        this.loginDataSource = loginDataSource;

    }


    @Override
    public void logIn(String userName, String pass,LogInCallback logInCallback) {

        Log.d(MyTAG,"login repo");

      loginDataSource.logIn(userName,pass,logInCallback);
    }
}
