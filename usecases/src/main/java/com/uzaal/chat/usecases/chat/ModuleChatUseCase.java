package com.uzaal.chat.usecases.chat;

import android.content.Context;
import android.support.annotation.NonNull;

import com.uzaal.chat.usecases.config.PreferenceHandler;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by root on 6/29/18.
 */

//@Module(includes = ModuleChatRepository.class)
@Module
public class ModuleChatUseCase {

      @Provides
      public SendMessage provideSendMessageUseCase(@NonNull ChatRepository messsageRepository) {

         return new SendMessage(messsageRepository);

      }

    @Provides
    public GetMessages provideGetMessageUseCase(@NonNull ChatRepository messsageRepository) {

        return new GetMessages(messsageRepository);

    }



    @Provides
    @Singleton
    public ChatRepository provideChatRepository(ChatServiceClient chatServiceClient, MessageDao messageDao){

        return new ChatRepository(chatServiceClient,messageDao);

    }



    @Provides
    @Singleton
    public ChatServiceClient provideSmackClient(Context context, PreferenceHandler preferenceHandler){

        return new ChatSmackServiceClient(context,preferenceHandler);

    }


    @Provides
    public MessageDao provideMesssageDaoRoom(){

        return new MessageDaoRoom();

    }





}
