package com.uzaal.chat.usecases;

/**
 * Created by root on 7/3/18.
 */

/**
 * Created by root on 5/24/18.
 */

//inputport , output port(answer)

//This is the InputBoundary
//3 parameters , 1 result
//These parameters are generic they will be defined by the classes that implement the interface
public interface UseCase<P extends UseCase.RequestValues,R extends UseCase.ResponseValue> {
    //means the parameters have to be of that type


    //The output boundary is an interface implemented by the delivery mechanism (ViewModel)
    //method which the use case will call on  in order to pass the
    //response model from the application to the delivery mechanism (viewModel)

    //This is the OutputBoundary //the delivery mechanism
    interface OutputBoundary<R> {

        void onSuccess(R result);

        void onNetworkUnAvailable();

        void onInvalidMessage();


    }


    //The Input boundary is implemented by the usecase
    //Defines requests and methods required by the usecase
    //which The delivery mechanism (ViewModel) will use to execute the usecase
    // TheViewmodel (delivery mech)  will say usecase.execute()


    //(input parameters, request)
    //void execute(String operator, int operand1,int operand2, OutputBoundary<R> delivery);
    void execute(P param, OutputBoundary<R> delivery);

// The use case implements the input boundary and uses the output boundary.




    /**
     * Data passed to a request.
     */
    interface RequestValues {
    }

    /**
     * Data received from a request.
     */
    interface ResponseValue {
    }

}