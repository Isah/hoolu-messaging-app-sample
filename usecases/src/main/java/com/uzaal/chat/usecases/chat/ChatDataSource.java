package com.uzaal.chat.usecases.chat;


import com.uzaal.chat.usecases.entities.HooluMessage;

import java.util.List;

/**
 * Created by root on 7/3/18.
 */

public interface ChatDataSource {



    interface ConnectionCallback{


        void onConnected();
        void onConnectionFailed();


    }



    //sending message
    interface SendMessageCallback{
     void onMessageSent();
     void onMessageFailed();
    }

    interface SaveMessagesCallback{
        void onMessageSaved(String messages);
        void onSaveFailed();
    }
    void saveMessage(HooluMessage hooluMessage, SaveMessagesCallback saveMessagesCallback);
    void sendMessage(HooluMessage hooluMessage, SendMessageCallback sendMessageCallback);



    //get messagaes

    interface GetMessageCallback{
        void onMessageLoaded(HooluMessage hooluMessages);
        void onDataNotAvailable();
    }
    interface GetMessagesCallback{
        void onMessageLoaded(List<HooluMessage> hooluMessages);
        void onDataNotAvailable();
    }
    void getMessage(String userId, GetMessageCallback getMessageCallback);
    void getMessages(String userId, GetMessagesCallback getMessageCallback);









    /*

    interface LogInCallback{
        void onUserLogIn();
        void onLogInFailed();
    }
    void logIn(LogInCallback logInCallback);




    interface RegisterCallback{

        void onUserRegistered(User user);
        void onRegistrationFailed();
        void onConnectionFailed();
    }
    void register(String userName, String password, RegisterCallback registerCallback);
    */

    void connect(ConnectionCallback connectionCallback);








}
