package com.uzaal.chat.usecases.config;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.google.gson.reflect.TypeToken;
import com.uzaal.chat.usecases.entities.User;

import java.lang.reflect.Type;

import javax.inject.Inject;

public class PreferenceHandler {

    private static PreferenceHandler instance;
    private Context mContext;


    @Inject
    public PreferenceHandler(Context context) {
        mContext = context;
    }


    public void putPref(String key, String value) {

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(mContext);
        SharedPreferences.Editor editor = prefs.edit();


        editor.putString(key, value);


        editor.apply();
    }

    public String getPref(String key) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(mContext);
        return preferences.getString(key, null);
    }


    public Boolean isUserRegistered(String key) {


        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(mContext);
        String value = preferences.getString(key, null);

        boolean b = Boolean.parseBoolean(value);
        return b;

    }


    private String userPref = "userPref";

    public User getUserFromPreferences() {


        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(mContext);

        User user = new User();
        String nameLabel = userPref+"_username";
        String passLabel = userPref+"_password";



        user.setUsername(prefs.getString(nameLabel, null));
        user.setPassword(prefs.getString(passLabel, null));
        user.setEmail(prefs.getString("email", null));
      //  user.setEmail((prefs.getInt("email", 0));

        //  Set<String> catogryItems = prefs.getStringSet("categories",null);


        return user;

    }


    public void setUserPrefence(User user) {
        //Initialize SharedPreferences
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(mContext);

        String nameLabel = userPref+"_username";
        String passLabel = userPref+"_password";

        //make anew preference editor
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(nameLabel, user.getUsername());
        editor.putString(passLabel, user.getPassword());
        editor.putString("email", user.getEmail());
      //  editor.putInt("year", destination.getYear());
        editor.apply();


    }


    public void removePreference(String key, Context context) {

        //Initialize SharedPreferences
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);

        //make anew preference editor
        SharedPreferences.Editor editor = prefs.edit();
        //        editor.putString("faculty",myPreference.getDestination().getFaculty());
        //      editor.putString("course",myPreference.getDestination().getCourseName());
        //    editor.putString("year",myPreference.getDestination().getYear());
        editor.remove(key);


        editor.apply();

        editor.clear();


    }


    public void setIntValue(String key, int value) {

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(mContext);
        SharedPreferences.Editor editor = prefs.edit();


        editor.putInt(key, value);


        editor.apply();
    }

    public int getIntValue(String key) {


        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(mContext);
        int value = preferences.getInt(key, 0);


        return value;

    }


}