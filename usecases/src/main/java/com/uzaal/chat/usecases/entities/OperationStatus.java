package com.uzaal.chat.usecases.entities;

/**
 * Created by root on 7/5/18.
 */

public enum OperationStatus {
      SENT,
      SENT_READ,
      FAILED,
      PENDING,
      RECEIVED,
      INVALID
   }

