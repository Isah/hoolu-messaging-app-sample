package com.uzaal.chat.usecases.chat;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.Handler;
import android.os.IBinder;
import android.support.annotation.RestrictTo;
import android.util.Log;


import com.uzaal.chat.usecases.config.ConnectionConfig;
import com.uzaal.chat.usecases.entities.HooluMessage;
import com.uzaal.chat.usecases.utilities.AppExecutors;

import org.jivesoftware.smack.SmackException;
import org.jivesoftware.smack.chat2.Chat;
import org.jivesoftware.smack.chat2.ChatManager;
import org.jivesoftware.smack.chat2.IncomingChatMessageListener;
import org.jivesoftware.smack.packet.Message;
import org.jxmpp.jid.EntityBareJid;
import org.jxmpp.jid.impl.JidCreate;
import org.jxmpp.stringprep.XmppStringprepException;

import java.util.Date;

/**
 * Created by root on 7/3/18.
 */

public class ChatSmackService extends Service {


    private String userName;
    private String passWord;
    private String message;
    private String from;
    private String to;
    private String HOST;
    private String HOST_ADDRESS;


   // private ChatXmpp myXMPP = ChatXmpp.getInstance();
    private AppExecutors appExecutors = new AppExecutors();


    @RestrictTo (RestrictTo.Scope.TESTS)
    public boolean started = false;

    @RestrictTo (RestrictTo.Scope.TESTS)
    public boolean stopped = false;

    @RestrictTo (RestrictTo.Scope.TESTS)
    public boolean isOnStartCommandStarted = false;


    //1. Binder given to clients to bind to services
    private final IBinder mBinder = new LocalBinder();


    /**2
     * Class used for the client Binder.  Because we know this service always
     * runs in the same process as its clients, we don't need to deal with IPC.
     */
    public class LocalBinder extends Binder {
       public ChatSmackService getService() {
            // Return this instance of LocalService so clients can call public methods
            return ChatSmackService.this;
        }
    }


    // To provide binding for a service i need to implement onBind()
    // returns a IBinder that provides a programming interface that clients
    // can use to interact with the service

    @Override
    public IBinder onBind(Intent intent) {

        if(intent != null){

            passWord = intent.getStringExtra("pwd");
            userName = intent.getStringExtra("usr");
            HOST = intent.getStringExtra("host");
            HOST_ADDRESS = intent.getStringExtra("host_address");

            Log.d("SERVICE CONN BOUND: ", "time: "+new Date());


        }
        return mBinder;
    }



    private boolean mIsConnectionEstablished = false;



    public interface OnConnectionEstablishedListener {
        void onConnectionEstablished();
    }
    private OnConnectionEstablishedListener mListener;

    //the one that impl this interface will get the call back
    public void setOnConnectionEstablishedListener(OnConnectionEstablishedListener listener) {

        mListener = listener;

        // Already connected to server. Notify immediately.
        if(mIsConnectionEstablished) {
            mListener.onConnectionEstablished();
        }
    }





    private void notifyConnectionEstablished() {
        mIsConnectionEstablished = true;

        if(mListener != null) {
            mListener.onConnectionEstablished();
        }
    }






    @Override
    public void onCreate() {
        super.onCreate();
        started = true;

        final Handler handler = new Handler();
        Runnable runnable = new Runnable() {
            @Override
            public void run() {

                Log.d("SERVICE CONN EST: ", "time: "+new Date());

                notifyConnectionEstablished();


            }
        };
        handler.postDelayed(runnable,5000);

        appExecutors.diskIO().execute(runnable);


    }

    @Override
    public void onDestroy() {
       // myXMPP.disconnectConnection();
        super.onDestroy();
        stopped = true;
    }



    ChatManager chatmanager;
    Chat newChat;
    ConnectionConfig myXMPPConnection = ConnectionConfig.getInstance();



    public void sendMessage(HooluMessage hooluMessage,  final ChatDataSource.SendMessageCallback sendMessageCallback) {
        //This is where the retrofit or xmpp msg client would come in
        //If (myXMPP.isConnected()  && myXMPP.login()) {
            //check login

            Log.d("MSG SEND", "ACTIVE");

            EntityBareJid entityBareJid = null;

            try {

               entityBareJid = JidCreate.entityBareFrom(hooluMessage.getTo());

            }catch (XmppStringprepException x){ }

            Message message = new Message();
            message.setBody(hooluMessage.getMessage());
            message.setSubject("Just Chatting");
            message.setTo(entityBareJid);


            try {


                chatmanager = ChatManager.getInstanceFor(myXMPPConnection.getConnection());

                Log.d("MSG SEND", "IN QUEUE");

                try {
                    //  EntityJid my_entity_jid =  mConnection.getUser();
                    EntityBareJid my_entity_jid = message.getTo().asEntityBareJidIfPossible();
                    Log.d("MSG SEND", "IN ENTITY JID " + my_entity_jid);

                    newChat = chatmanager.chatWith(my_entity_jid);
                    newChat.send(message);

                    sendMessageCallback.onMessageSent();


                } catch (SmackException.NotConnectedException e) {
                    e.printStackTrace();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }


            }catch (Exception e) {

                sendMessageCallback.onMessageFailed();

            }



    }

    public void getMessage(String userId, final ChatDataSource.GetMessageCallback getMessageCallback) {

           IncomingChatMessageListener messageListener =  new IncomingChatMessageListener() {
                    @Override
                    public void newIncomingMessage(EntityBareJid from, Message message, Chat chat) {


                        Log.d("HOOLU_MESSAGE","msg: "+message.getBody());


                        HooluMessage hooluMessage = new HooluMessage();
                        hooluMessage.setMessage(message.getBody());
                        hooluMessage.setDeliveryStatus(HooluMessage.DeliveryStatus.FROM);
                        hooluMessage.setTimeStamp(new Date());
                        hooluMessage.setFrom(from.asEntityBareJidString());

                        getMessageCallback.onMessageLoaded(hooluMessage);

                    }
                };

        chatmanager = ChatManager.getInstanceFor(myXMPPConnection.getConnection());
        chatmanager.addIncomingListener(messageListener);


            }







}





