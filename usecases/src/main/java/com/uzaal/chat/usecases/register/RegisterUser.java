package com.uzaal.chat.usecases.register;

import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;

import com.uzaal.chat.usecases.UseCase;
import com.uzaal.chat.usecases.entities.HooluMessage;
import com.uzaal.chat.usecases.entities.User;

import javax.inject.Inject;


/**
 * Created by root on 6/29/18.
 */

public class RegisterUser implements UseCase<RegisterUser.RequestValues, RegisterUser.ResponseValue> {


    RegisterRepository loginRepository;

    @Inject
    public RegisterUser(RegisterRepository loginRepository){

    this.loginRepository = loginRepository;

    }



    //specify what the visibility sjould have been if not for testin
   @VisibleForTesting(otherwise = VisibleForTesting.PROTECTED)
    public static ResponseValue responseValue;





    @Override
    public void execute(RequestValues requestValues, final OutputBoundary<ResponseValue> deliveryMechanism) {

        //somebody else will implement deliverMech not me so ill just call it
        //but ill implement the sending (new) coz i need the results

        // mat:8:16-17,josh:10:5-8
        // num 25:6-9

        /* we need to attack the user service and register there
           then if successful we get the initials and register in the ejabberd server

           then the payment system will send the notification to the ejabberd to send the
           msg to the user registered on a device

         */




        String userName = requestValues.getUserName();
        String password = requestValues.getPassword();


        loginRepository.register(userName, password, new RegisterServiceClient.RegisterCallback() {
            @Override
            public void onUserRegistered(User user) {
                deliveryMechanism.onSuccess(new ResponseValue(user));

            }

            @Override
            public void onRegistrationFailed() {
                deliveryMechanism.onNetworkUnAvailable();
            }

            @Override
            public void onConnectionFailed() {

            }
        });




    }





    //Validator usecase
    private boolean isValid(HooluMessage hooluMessage){

        if(hooluMessage.getMessage().isEmpty()){

            return false;

        }
        else if(hooluMessage.getMessage().length() > 30) {

        return false;

        }

        else{

            return true;
        }

    }




    //it implements such that its of a right type to be accepted in thhe execute method
    public static final class RequestValues implements UseCase.RequestValues {

        private final String userName;
        private final String password;

        public RequestValues(@NonNull String userName, String password) {
            this.userName = userName;
            this.password = password;

        }

        public String getUserName() {
            return userName;
        }

        public String getPassword() {
            return password;
        }


    }

    public static final class ResponseValue implements UseCase.ResponseValue {

        private final User user;

        public ResponseValue(@NonNull User user) {
            this.user = user;
        }

        public User getUser() {
            return user;
        }



    }





}
