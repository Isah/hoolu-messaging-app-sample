package com.uzaal.chat.usecases.chat;

import com.uzaal.chat.usecases.entities.HooluMessage;

interface ChatServiceClient {



    void sendMessage(HooluMessage hooluMessage, final ChatDataSource.SendMessageCallback sendMessageCallback);
    void getMessage(String userid, final ChatDataSource.GetMessageCallback getMessageCallback);




}
