package com.uzaal.chat.usecases.chat;

import android.support.annotation.NonNull;


import com.uzaal.chat.usecases.UseCase;
import com.uzaal.chat.usecases.entities.HooluMessage;

import javax.inject.Inject;

/**
 * Created by root on 7/5/18.
 */

public class GetMessages implements UseCase<GetMessages.RequestValues, GetMessages.ResponseValue> {


    ChatRepository messsageRepository;
    @Inject
    public GetMessages(ChatRepository messsageRepository){
        this.messsageRepository = messsageRepository;
    }



    @Override
    public void execute(RequestValues param, final OutputBoundary<ResponseValue> delivery) {

        String userid = param.getmUserID();

        messsageRepository.getMessage(userid, new ChatDataSource.GetMessageCallback() {
            @Override
            public void onMessageLoaded(HooluMessage hooluMessage) {

                hooluMessage.setDeliveryStatus(HooluMessage.DeliveryStatus.FROM);
                ResponseValue responseValue = new ResponseValue(hooluMessage);

                delivery.onSuccess(responseValue);

            }

            @Override
            public void onDataNotAvailable() {

                delivery.onNetworkUnAvailable();

            }
        });
    }



    //it implements such that its of a right type to be accepted in thhe execute method
    public static final class RequestValues implements UseCase.RequestValues {

        private final String userID;

        public RequestValues(@NonNull String userid) {
            this.userID = userid;
        }

        public String getmUserID() {
            return userID;
        }
    }

    public static final class ResponseValue implements UseCase.ResponseValue {

        private HooluMessage mHooluMessages;

        public ResponseValue(@NonNull HooluMessage mHooluMessages) {
            this.mHooluMessages = mHooluMessages;
        }

        public HooluMessage getmHooluMessage() {
            return mHooluMessages;
        }
    }




}
