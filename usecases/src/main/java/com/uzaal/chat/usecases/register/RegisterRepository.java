package com.uzaal.chat.usecases.register;


import javax.inject.Inject;

import static dagger.internal.Preconditions.checkNotNull;

/**
 * Created by root on 6/29/18.
 */

class RegisterRepository implements RegisterServiceClient {

    //The client that connects to Android Service sends the messages
    // SmackXmppClient,SomeOtherXmppClient
    private RegisterServiceClient loginDataSource;//there can be different login srcs (smack)
    private static RegisterRepository INSTANCE = null;


    @Inject
    public RegisterRepository(RegisterServiceClient loginDataSource){
        checkNotNull(loginDataSource);
        this.loginDataSource = loginDataSource;

    }

    @Override
    public void register(String userName, String password, RegisterCallback registerCallback) {

        loginDataSource.register(userName,password,registerCallback);
    }


}
