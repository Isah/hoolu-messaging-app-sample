package com.uzaal.chat.usecases.login;


/**
 * Created by root on 7/3/18.
 */

public interface LoginServiceClient {


   interface LogInCallback{
        void onUserLogIn();
        void onLogInFailed();
    }
    void logIn(String userName, String pass, LogInCallback logInCallback);



}
