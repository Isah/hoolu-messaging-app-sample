package com.uzaal.chat.usecases.chat;

import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;

import com.uzaal.chat.usecases.UseCase;
import com.uzaal.chat.usecases.entities.HooluMessage;

import javax.inject.Inject;

/**
 * Created by root on 6/29/18.
 */

public class SendMessage implements UseCase<SendMessage.RequestValues, SendMessage.ResponseValue> {


    ChatRepository messsageRepository;

    @Inject
    public SendMessage(ChatRepository messsageRepository){

    this.messsageRepository = messsageRepository;

    }



    //specify what the visibility sjould have been if not for testin
   @VisibleForTesting(otherwise = VisibleForTesting.PROTECTED)
    public static ResponseValue responseValue;





    @Override
    public void execute(RequestValues requestValues, final OutputBoundary<ResponseValue> deliveryMechanism) {

        //somebody else will implemtnt deliverMech not me so ill just call it
        //but ill implement the sending (new) coz i need the results

        final HooluMessage hooluMessage = requestValues.getmHooluMessage();

        if(isValid(hooluMessage)) {

            messsageRepository.sendMessage(hooluMessage, new ChatDataSource.SendMessageCallback() {
                @Override
                public void onMessageSent() {

                    responseValue = new ResponseValue(hooluMessage);
                    deliveryMechanism.onSuccess(responseValue);
                }


                @Override
                public void onMessageFailed() {


                    deliveryMechanism.onNetworkUnAvailable();


                }
            });

        }
        else {

            deliveryMechanism.onInvalidMessage();
        }
    }





    //Validator usecase
    private boolean isValid(HooluMessage hooluMessage){

        if(hooluMessage.getMessage().isEmpty()){

            return false;

        }
        else if(hooluMessage.getMessage().length() > 30) {

        return false;

        }

        else{

            return true;
        }

    }




    //it implements such that its of a right type to be accepted in thhe execute method
    public static final class RequestValues implements UseCase.RequestValues {

        private final HooluMessage mHooluMessage;

        public RequestValues(@NonNull HooluMessage hooluMessage) {
            mHooluMessage = hooluMessage;
        }

        public HooluMessage getmHooluMessage() {
            return mHooluMessage;
        }
    }

    public static final class ResponseValue implements UseCase.ResponseValue {

        private HooluMessage mHooluMessage;

        public ResponseValue(@NonNull HooluMessage mHooluMessage) {
            this.mHooluMessage = mHooluMessage;
        }

        public HooluMessage getmHooluMessage() {
            return mHooluMessage;
        }



    }





}
