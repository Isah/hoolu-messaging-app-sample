package com.uzaal.chat.usecases.config;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.VisibleForTesting;
import android.util.Log;


import org.jivesoftware.smack.ConnectionConfiguration;
import org.jivesoftware.smack.ConnectionListener;
import org.jivesoftware.smack.ReconnectionManager;
import org.jivesoftware.smack.SmackConfiguration;
import org.jivesoftware.smack.SmackException;
import org.jivesoftware.smack.XMPPConnection;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smack.tcp.XMPPTCPConnection;
import org.jivesoftware.smack.tcp.XMPPTCPConnectionConfiguration;
import org.jivesoftware.smack.util.StringUtils;
import org.jivesoftware.smack.util.TLSUtils;
import org.jivesoftware.smackx.ping.PingFailedListener;
import org.jivesoftware.smackx.ping.PingManager;
import org.jxmpp.jid.DomainBareJid;
import org.jxmpp.jid.impl.JidCreate;
import org.jxmpp.stringprep.XmppStringprepException;

import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;

public class ConnectionConfig {


    private static final String TAG = "RoosterService";
    private boolean mActive;//Stores whether or not the thread is active
    private Thread mThread;
    private Handler mTHandler;//We use this handler to post messages to
    //The background thread.

    private String userName = "";
    private String passWord = "";


    public MyXMPPConnectionListener connectionListener = new MyXMPPConnectionListener();

    //ca be accessed for testing
    @VisibleForTesting(otherwise = VisibleForTesting.PRIVATE)
    public static boolean connected;


    boolean isToasted;
    boolean chat_created;
    boolean loggedin;


    public static final int PORT = 5222;
    InetAddress inetAddress;
    public static Context mApplicationContext;
    public  XMPPTCPConnection mConnection;


    private static ConnectionConfig instance;


    private ConnectionConfig() {

    }

    public static ConnectionConfig getInstance() {
        if (instance == null) {
            synchronized (ConnectionConfig.class) {
                if (instance == null) {
                    instance = new ConnectionConfig();
                }
            }
        }
        return instance;
    }




    private static final byte[] dataToSend = StringUtils.randomString(1024 * 4 * 3).getBytes();


    private XMPPTCPConnectionConfiguration.Builder config;
    DomainBareJid serviceName;

    private String mUserName;
    private String mPassWord;


    public void setUserOnConnection(String userName, String passWord){
      this.mUserName = userName;
      this.mPassWord =passWord;
    }

    public void configureConnection(String host, String hostAddress) {
      //  this.userName = mUsername;
      //  this.passWord = mPassword;


        Log.i(TAG, "Initialisation");
        config = XMPPTCPConnectionConfiguration.builder();


        try {
            serviceName = JidCreate.domainBareFrom(host);
            inetAddress = InetAddress.getByName(hostAddress);
        } catch (UnknownHostException | XmppStringprepException e) {

        }

        // config.setServiceName(DOMAIN); //username of the pc
        config.setSecurityMode(ConnectionConfiguration.SecurityMode.ifpossible);
        config.setXmppDomain(serviceName);
        config.setHostAddress(inetAddress);
        config.setPort(PORT);
        config.setCompressionEnabled(false);
        config.setDebuggerEnabled(true);
        config.setSendPresence(true);


        config.setConnectTimeout(15000);
        config.setUsernameAndPassword(mUserName, mPassWord);

        //XMPPTCPConnection.setUseStreamManagementResumptiodDefault(true);
        //XMPPTCPConnection.setUseStreamManagementDefault(true);

        try {
            TLSUtils.acceptAllCertificates(config);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (KeyManagementException e) {
            e.printStackTrace();
        }


        mConnection = new XMPPTCPConnection(config.build());
        mConnection.addConnectionListener(connectionListener);






        ReconnectionManager reconnectionManager = ReconnectionManager.getInstanceFor(mConnection);
        reconnectionManager.enableAutomaticReconnection();
        ReconnectionManager.setEnabledPerDefault(true);


        PingManager.getInstanceFor(mConnection).registerPingFailedListener(new PingListner());

        startConnecting();

    }



      //PING //////////////////////////////////////////////////////////////


    public class PingListner implements PingFailedListener{

        @Override
        public void pingFailed() {

            Log.d("Png Failed", "pinging failed");
            //  assertEquals(expectedUserJid, packet.getFrom().asBareJid().toString());


        }
    }

    public void addPingListner(PingFailedListener pingFailedListener) {
        PingManager.getInstanceFor(mConnection).registerPingFailedListener(pingFailedListener);
    }








    /*
    private XMPPTCPConnection getConnectionFromConfig() throws XMPPException, SmackException, IOException, InterruptedException {
        Log.d(TAG, "Getting XMPP Connect");
        if (isConnected()) {
            Log.d(TAG, "Returning already existing connection");
            return this.mConnection;
        }

        long l = System.currentTimeMillis();
        try {
            if(this.mConnection != null){

                Log.d(TAG, "Connection found, trying to connect");
                this.mConnection.connect();

            }else{
                Log.d(TAG, "No Connection found, trying to create a new connection");


               // XMPPTCPConnectionConfiguration config = buildConfiguration();

                SmackConfiguration.DEBUG = true;

              //  this.mConnection = new XMPPTCPConnection(config);
              //  this.mConnection.connect();
            }
        } catch (Exception e) {
            Log.e(TAG,"some issue with getting connection :" + e.getMessage());

        }

        Log.d(TAG, "Connection Properties: " + mConnection.getHost() + " " + mConnection.getServiceName());
        Log.d(TAG, "Time taken in first time connect: " + (System.currentTimeMillis() - l));
        return this.mConnection;
    }
     */


    public XMPPConnection getConnection() {
        return mConnection;
    }

    public XMPPTCPConnection getTcpConnection() {
        return mConnection;
    }



    // Disconnect Function
    public void disconnectConnection(){


        new Thread(new Runnable() {
            @Override
            public void run() {
                mConnection.disconnect();
            }
        }).start();
    }






    /*
    private XMPPTCPConnection getConnectionThatChecksLoginSanza() {
        Log.d(TAG, "Inside connect and Login");

        if (isConnected()) {
            Log.d(TAG, "Connection not connected, trying to login and connect");
            try {
                // Save username and password then use here

                Log.d(TAG, "BEFORE CONN For Stanza username :" + userName);
                Log.d(TAG, "BEFORE CONN For Stanza password :" + passWord);
                mConnection.login(userName+"@localhost", passWord);


                Log.d(TAG, "Connect and Login method, Login successful");
                //context.sendBroadcast(new Intent(ACTION_LOGGED_IN));

            } catch (XMPPException localXMPPException) {
                Log.e(TAG, "Error in Connect and Login Method");
                localXMPPException.printStackTrace();
            } catch (SmackException e) {
                Log.e(TAG, "Error in Connect and Login Method");
                e.printStackTrace();
            } catch (IOException e) {
                Log.e(TAG, "Error in Connect and Login Method");
                e.printStackTrace();
            } catch (InterruptedException e) {
                Log.e(TAG, "Error in Connect and Login Method");
                e.printStackTrace();
            } catch (IllegalArgumentException e) {
                Log.e(TAG, "Error in Connect and Login Method");
                e.printStackTrace();
            } catch (Exception e) {
                Log.e(TAG, "Error in Connect and Login Method");
                e.printStackTrace();
            }
        }
        Log.i(TAG, "Inside getConnection - Returning connection");
        return mConnection;
    }
     */



    //Connection Listener to check connection state
    public class MyXMPPConnectionListener implements ConnectionListener {
        @Override
        public void connected(final XMPPConnection connection) {

            Log.d("XMPP CONNECTION LISTNER", "Connected!");
            connected = true;


            if (!connection.isAuthenticated()) {
               // login();
            }
        }

        @Override
        public void connectionClosed() {
            if (isToasted)

                new Handler(Looper.getMainLooper()).post(new Runnable() {

                    @Override
                    public void run() {
                        // TODO Auto-generated method stub


                    }
                });
            Log.d("xmpp", "ConnectionCLosed!");
            connected = false;
            chat_created = false;
            loggedin = false;

        }

        @Override
        public void connectionClosedOnError(Exception arg0) {
            if (isToasted)

                new Handler(Looper.getMainLooper()).post(new Runnable() {

                    @Override
                    public void run() {

                    }
                });

            Log.d("xmpp", "ConnectionClosedOn Error!");
            connected = false;


            chat_created = false;
            loggedin = false;
        }

        @Override
        public void reconnectingIn(int arg0) {

            Log.d("xmpp", "Reconnectingin " + arg0);

            loggedin = false;
        }

        @Override
        public void reconnectionFailed(Exception arg0) {
            if (isToasted)

                new Handler(Looper.getMainLooper()).post(new Runnable() {

                    @Override
                    public void run() {



                    }
                });
            Log.d("xmpp", "ReconnectionFailed!");
            connected = false;

            chat_created = false;
            loggedin = false;
        }

        @Override
        public void reconnectionSuccessful() {
            if (isToasted)

                new Handler(Looper.getMainLooper()).post(new Runnable() {

                    @Override
                    public void run() {
                        // TODO Auto-generated method stub



                    }
                });
            Log.d("xmpp", "ReconnectionSuccessful");
            connected = true;

            chat_created = false;
            loggedin = false;
        }

        @Override
        public void authenticated(XMPPConnection arg0, boolean arg1) {
            Log.d("xmpp", "Authenticated!");
            loggedin = true;

            chat_created = false;
            new Thread(new Runnable() {

                @Override
                public void run() {
                    try {
                        Thread.sleep(500);
                    } catch (InterruptedException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }

                }
            }).start();
            if (isToasted)

                new Handler(Looper.getMainLooper()).post(new Runnable() {

                    @Override
                    public void run() {
                        // TODO Auto-generated method stub



                    }
                });
        }
    }


    public boolean isConnected() {
        return (mConnection != null) && (mConnection.isConnected());
    }

    //start connection and login used by my_xmpp_service

    private boolean startConnecting() {
        Log.d(TAG," StartConnection function called.");



        if(!mActive) {

            mActive = true;
            if( mThread ==null || !mThread.isAlive())
            {
                mThread = new Thread(new Runnable() {
                    @Override
                    public void run() {

                        Looper.prepare();
                        mTHandler = new Handler();

                        // Create a connection


                            if (isConnected()) {
                                Log.d(TAG, "Returning already existing connection");



                            }else{


                                try {

                                    Log.d(TAG, "NO CONNECTION FOUND, trying to create a new connection");


                                    SmackConfiguration.DEBUG = true;

                                    mConnection.connect();

                                    Log.d(TAG, "CONNECTION CREATED "+mConnection.getStreamId());

                                    connected = true;



                                } catch (IOException | SmackException | XMPPException | InterruptedException e) {




                                    connected = false;

                                    Log.i("CONNECTION EXCEPTION", "Something Happened!");



                                    e.printStackTrace();

                                }




                            }

                        //THE CODE HERE RUNS IN A BACKGROUND THREAD.
                        Looper.loop();

                    }
                });

                mThread.start();
            }


        }

        Log.d(TAG, "Connection Properties: " + mConnection.getHost() + " " + mConnection.getServiceName());


        return connected;

    }

    public void stop() {
        Log.d(TAG,"stop()");
        mActive = false;
        mTHandler.post(new Runnable() {
            @Override
            public void run() {
                if( mConnection != null)
                {
                    mConnection.disconnect();
                }
                //CODE HERE IS MEANT TO SHUT DOWN OUR CONNECTION TO THE SERVER.

            }
        });

    }





}
