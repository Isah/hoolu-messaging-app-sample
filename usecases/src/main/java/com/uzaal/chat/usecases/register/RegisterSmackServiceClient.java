package com.uzaal.chat.usecases.register;


import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.util.Log;
import com.uzaal.chat.usecases.config.HostConfig;
import javax.inject.Inject;

/**
 * Created by root on 7/3/18.
 */

class RegisterSmackServiceClient implements RegisterSmackService.OnConnectionEstablishedListener, RegisterServiceClient{


    /***
     * Job: Execute the  BinderService tasks
     * 1. start the service
     * 2. invoke login, send msg, get msg
     * 3. all those invocations happen after a connection has been established
     *    that's why we have the connection listener
     */



    Context myContext;

    //my intent service needs Myxmpp
    private RegisterSmackService binderService;
    boolean mBound = false;


    // Prevent direct instantiation.
    @Inject
    public RegisterSmackServiceClient(Context myContext) {
        this.myContext = myContext;

    }


    public void bindtoLocalService(String userName, String pass, RegisterCallback logInCallback){

         // myContext =  preferenceHandler.getActivityContext();
         // Bind to LocalService
         Intent intent = new Intent(myContext, RegisterSmackService.class);
        // User user = HostConfig.getUser(myContext);
         intent.putExtra("usr",userName);
         intent.putExtra("pwd",pass);
         intent.putExtra("host", HostConfig.HOST);
         intent.putExtra("host_address",HostConfig.HOST_ADDRESS);

         Log.d("Bound Service Before", ""+myContext.getPackageName());

        // application.startService(intent);
        myContext.bindService(intent, new ServiceConnection() {
            @Override
            public void onServiceConnected(ComponentName componentName, IBinder iBinder) {

                //LocalBinder binder = (LocalBinder) iBinder;
                binderService = ((RegisterSmackService.LocalBinder) iBinder).getService();

                Log.d("Bound Service Conn", ""+ binderService.started);

                //binderService = (MyXMPPBinderService) binder.getService();
                if(binderService != null){
                    Log.d("service-bind", "Service is bonded successfully!");
                    mBound = true;
                    binderService.setOnConnectionEstablishedListener(RegisterSmackServiceClient.this);
                    //do whatever you want to do after successful binding

                    binderService.registerUser(userName,pass,logInCallback);

                }else {

                    Log.d("service-bind", "Service is not bonded !");

                }


            }

            @Override
            public void onServiceDisconnected(ComponentName componentName) {

            }
        }, Context.BIND_AUTO_CREATE);

        Log.d("Bound Service After", ""+myContext.getPackageName());


    }

    private boolean isConnectionEstablished = false;
    protected boolean isXmppConnectionEstablished = false;


    @Override
    public void onConnectionEstablished() {

        // At this point the service has been bound and connected to the server
        // Do stuff here
        // Note: This method is called from a non-UI thread.
            isConnectionEstablished = true;

    }



    /** Defines callbacks for service binding, passed to bindService() */

    @Override
    public void register(String userName, String password, RegisterCallback registerCallback) {
        if(!mBound) {
            bindtoLocalService(userName,password,registerCallback);
        }
    }
}
