package com.uzaal.chat.usecases;


import com.uzaal.chat.usecases.UseCase;
import com.uzaal.chat.usecases.chat.ChatDataSource;
import com.uzaal.chat.usecases.chat.ChatRepository;
import com.uzaal.chat.usecases.chat.SendMessage;
import com.uzaal.chat.usecases.entities.HooluMessage;


import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.isNotNull;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;

/**
 * Created by root on 7/3/18.
 */




 //-----

@RunWith(MockitoJUnitRunner.class)
public class SendHooluMessageUseCaseTest {

    @Mock
    ChatRepository repositoryMock;

    @Mock
    UseCase.OutputBoundary<SendMessage.ResponseValue> outputBoundaryMock;


    @Mock
    UseCase.OutputBoundary presenterOutputBoundary;


    @Mock
    ChatDataSource.SendMessageCallback sendMessageDataSourceCallbackMock;


    /**
     * {@link ArgumentCaptor} is a powerful Mockito API to capture argument values and use them to
     * perform further actions or assertions on them.
     */
    @Captor
    private ArgumentCaptor<ChatDataSource.SendMessageCallback> mSendMessageCallbackCaptor;

    @Captor
    private ArgumentCaptor<HooluMessage> messageArgumentCaptor;


    SendMessage sendMessage;


    @Before
    public void setUp(){

        MockitoAnnotations.initMocks(this);

        sendMessage = new SendMessage(repositoryMock);

    }

    /**
     * Equivalence class partitions
     * Testing for Invalid Inputs ( 3 equivalence classes )
     * 1. valid -  <= 30
     * 2. invalid - > 30
     * 3. invalid - empty message
     */


    /**
     TC1 - verify send msg with valid data
     Test Scenario -  send message successfully -> basic flow
     Preconditions -  both parties online
     Test Steps    - 1. enter msg,  2. click send button
     TestData      - ("Hello friend") <= 30 chars
     Expected      - msg sending gets executed
     */


    //ExpectedOutcome, Scenario Tested
    //Verifying Interactions
    @Test
    public void shouldSendMessageIfDataIsValid() throws Exception {
        //Need to mock the repository
        //Execute the use case and just capture its input callback parameters
        //Verify the repository methods were executed succesfully after the use case is called
        //Verify the output boundary methods are excuted
        //you don not need to have the web running or DB running
        //then check the ResponseModel

          HooluMessage dummyHooluMessage = new HooluMessage();
          dummyHooluMessage.setMessage("Hello World");

        //1. When send message is called
        //The input values can be tested on the mock object that is triggered for null or other using captor
        sendMessage.execute(new SendMessage.RequestValues(dummyHooluMessage),presenterOutputBoundary);

        //2. verify the right mtd executed
        Mockito.verify(repositoryMock).sendMessage(messageArgumentCaptor.capture(), mSendMessageCallbackCaptor.capture());


        //Make sure msg is captured before sending operation
        assertThat(messageArgumentCaptor.getValue().getMessage(), is(dummyHooluMessage.getMessage()));
        //assertThat(messageArgumentCaptor.getValue().getMessage(), isNotNull());

        // When msg is finally sent
        // trigger the  callback  //we do this coz we dont care or have msg sending functionality in repository
        mSendMessageCallbackCaptor.getValue().onMessageSent();


        //Check the Response
        //make sure the response value of the usecase that contains the dummy data is the one
        //received by the viewmodel

       // verify(viewModel).onSuccess(SendMessage.responseValue);

        assertThat(dummyHooluMessage, is(SendMessage.responseValue.getmHooluMessage()));

     //   assertThat(messageArgumentCaptor.getValue().getMessage(), is(viewModel.getmMessage().getValue()));


    }




    /**
     Test Scenario -  basic flow -> sys detects invalid message -> error msg is displayed
     TC2 - verify send msg with invalid data - empty message
     Preconditions -  both parties online
     Test Steps    - 1. enter msg,  2. click send button
     TestData      - ("") - empty string
     Expected      -  message sending fails error flagged
     */

    //Verifying Interactions
    @Test
    public void shouldFailToSendMessageIfDataIsEmpty(){
        //Need to mock the repository
        //Execute the use case and just capture its input callback parameters
        //Verify the repository methods were executed succesfully after the use case is called
        // e.g saveChild was executed

        HooluMessage dummyHooluMessage = new HooluMessage();
        dummyHooluMessage.setMessage("");  //empty string
       // dummyHooluMessage.setMessageID("");
        dummyHooluMessage.setUserID("");

        //1. When send message is called
        sendMessage.execute(new SendMessage.RequestValues(dummyHooluMessage),outputBoundaryMock);

        //2. verify the right mtd executed
        Mockito.verify(repositoryMock, never()).sendMessage(Mockito.any(HooluMessage.class), mSendMessageCallbackCaptor.capture());

        // When msg is finally sent
        // trigger the  callback
        // mSendMessageCallbackCaptor.getValue().onMessageSent();

        //if theres any kind of callback capture the values and simulate the display
    }



    /**
     Test Scenario -  basic flow -> sys detects invalid message -> error msg is displayed
     TC2 - verify send msg with invalid data - large message
     Preconditions -  both parties online
     Test Steps    - 1. enter msg,  2. click send button
     TestData      - ("How are you my friends hope you are doing well") - large string
     Expected      -  error flag is triggered
     */

    //Verifying Interactions
    @Test
    public void shoulFailToSendMessageIfDataExceedsLimitSize(){
        //Need to mock the repository
        //Execute the use case and just capture its input callback parameters
        //Verify the repository methods were executed succesfully after the use case is called
        // e.g saveChild was executed

        HooluMessage dummyHooluMessage = new HooluMessage();
        dummyHooluMessage.setMessage("How are you my friend are you okay today!");  //long string
       // dummyHooluMessage.setMessageID("");
        dummyHooluMessage.setUserID("");

        //1. When send message is called
        sendMessage.execute(new SendMessage.RequestValues(dummyHooluMessage),outputBoundaryMock);

        //2. verify the right mtd executed
        Mockito.verify(repositoryMock, never()).sendMessage(Mockito.any(HooluMessage.class), mSendMessageCallbackCaptor.capture());

        // Some assertion about the state before the callback is called
       //MessageDataSource.SendMessageCallback callback =  mSendMessageCallbackCaptor.getValue();

      // verify(callback, never()).onMessageSent();


        // When msg is finally sent
        // trigger the  callback
        // mSendMessageCallbackCaptor.getValue().onMessageSent();

        //if theres any kind of callback capture the values and simulate the display
    }





    /**
     Test Scenario -  basic flow -> sys detects un available network -> msg status pending
     TC2 - verify send msg with un available network - large message
     Preconditions -  both parties online
     Test Steps    - 1. enter msg,  2. click send button
     TestData      - ("How are you ") - large string
     Expected      - display pending message status
     */

    //Verifying Interactions
    @Test
    public void pendMessageIfThereisNoNetwork(){
        //Need to mock the repository
        //Execute the use case and just capture its input callback parameters
        //Verify the repository methods were executed succesfully after the use case is called
        // e.g saveChild was executed

        HooluMessage dummyHooluMessage = new HooluMessage();
        dummyHooluMessage.setMessage("How are you");  //long string
        //dummyHooluMessage.setMessageID("");
        dummyHooluMessage.setUserID("");

        //1. When send message is called
        sendMessage.execute(new SendMessage.RequestValues(dummyHooluMessage),outputBoundaryMock);

        //2. verify the right mtd executed
      //  Mockito.verify(repositoryMock).sendMessageOverNetwork(Mockito.any(Message.class), sendMessageDataSourceCallbackMock);

        // Some assertion about the state before the callback is called

    //    verify(sendMessageDataSourceCallbackMock, never()).onMessageFailed();


        // When msg is finally sent
        // trigger the  callback
        // mSendMessageCallbackCaptor.getValue().onMessageSent();

        //if theres any kind of callback capture the values and simulate the display
    }




    /**
     * Boundary Value Analysis
     * Testing for Boundary Values for each edge of equivalence classes
     * Upper Boundary
     * 1. eq valid <= 30 - at the boundary - 30
     * 2. eq invalid > 30  - just above boundary > 31
     *
     */



    /**
     Test Scenario -  send message successfully -> basic flow
     TC1 - verify send msg with valid boundary data 30
     Preconditions -  both parties online
     Test Steps    - 1. enter msg,  2. click send button
     TestData      - ("Hello good friend how are you?") just below - 30
     Expected      - msg sending gets executed
     */
    String message_at_boundary_30 = "Hello good friend how are you?";





    //Verifying Interactions
    @Test
    public void shouldSendMessageIfValidMessageSizeIsAtTheUpperBoundary() throws Exception {
        //Need to mock the repository
        //Execute the use case and just capture its input callback parameters
        //Verify the repository methods were executed succesfully after the use case is called
        // e.g saveChild was executed

        HooluMessage dummyHooluMessage = new HooluMessage();
        dummyHooluMessage.setMessage(message_at_boundary_30);

        //1. When send message is called
        sendMessage.execute(new SendMessage.RequestValues(dummyHooluMessage),outputBoundaryMock);

        //2. verify the right mtd executed
        Mockito.verify(repositoryMock).sendMessage(messageArgumentCaptor.capture(), mSendMessageCallbackCaptor.capture());

        // When msg is finally sent
        // trigger the  callback
        // mSendMessageCallbackCaptor.getValue().onMessageSent();

        //Make sure msg is captured before sending operation

        assertThat(messageArgumentCaptor.getValue().getMessage(), is(dummyHooluMessage.getMessage()));

        //if theres any kind of callback capture the values and simulate the display
    }




    /**
     TestCase1 - verify send msg with invalid boundary data 31
     Test Scenario -  basic flow -> sys detects invalid message -> error msg is displayed
     Preconditions -  both parties online
     Test Steps    - 1. enter msg,  2. click send button
     TestData      - ("Hello good friends how are you?") just above boundary - 31 chars
     Expected      - msg sending fails
     */
    String message_above_boundary_31 = "Hello good friends how are you?";




    //Verifying Interactions
    @Test
    public void shouldFailtoSendMessageIfInvalidMessageSizeIsJustAboveUpperBoundary() throws Exception {
        //Need to mock the repository
        //Execute the use case and just capture its input callback parameters
        //Verify the repository methods were executed succesfully after the use case is called
        // e.g saveChild was executed

        HooluMessage dummyHooluMessage = new HooluMessage();
        dummyHooluMessage.setMessage(message_above_boundary_31);

        //1. When send message is called
        sendMessage.execute(new SendMessage.RequestValues(dummyHooluMessage),outputBoundaryMock);

        //2. verify the right mtd executed
        Mockito.verify(repositoryMock, never()).sendMessage(messageArgumentCaptor.capture(), mSendMessageCallbackCaptor.capture());

        // When msg is finally sent
        // trigger the  callback
        // mSendMessageCallbackCaptor.getValue().onMessageSent();

        //Make sure msg is captured before sending operation

       // assertThat(messageArgumentCaptor.getValue().getMessage(), is(dummyMessage.getMessage()));

        //if theres any kind of callback capture the values and simulate the display
    }



    /*
    @Test
    public void getTasks_requestsAllTasksFromLocalDataSource() {
        // When tasks are requested from the tasks repository
        mTasksRepository.getTasks(mLoadTasksCallback);

        // Then tasks are loaded from the local data source
        verify(mTasksLocalDataSource).getTasks(any(TasksDataSource.LoadTasksCallback.class));
    }


    @Test
    public void saveTask_savesTaskToServiceAPI() {
        // Given a stub task with title and description
        Task newTask = new Task(TASK_TITLE, "Some Task Description");

        // When a task is saved to the tasks repository
        mTasksRepository.saveTask(newTask);

        // Then the service API and persistent repository are called and the cache is updated
        verify(mTasksRemoteDataSource).saveTask(newTask);
        verify(mTasksLocalDataSource).saveTask(newTask);
        assertThat(mTasksRepository.mCachedTasks.size(), is(1));
    }



    @Test
    public void saveTask_savesTaskToServiceAPI() {
        // Given a stub task with title and description
        Task newTask = new Task(TASK_TITLE, "Some Task Description");

        // When a task is saved to the tasks repository
        mTasksRepository.saveTask(newTask);

        // Then the service API and persistent repository are called and the cache is updated
        verify(mTasksRemoteDataSource).saveTask(newTask);
        verify(mTasksLocalDataSource).saveTask(newTask);
        assertThat(mTasksRepository.mCachedTasks.size(), is(1));
    }


    */



}






