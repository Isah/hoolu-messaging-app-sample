package com.uzaal.pwallet.controllers;

import com.uzaal.chat.usecases.UseCase;
import com.uzaal.chat.usecases.register.RegisterUser;

public class RegisterUserController {


    /**
     * Information Hiding
     * The Controller shouldn't know too much about the usecase
     * it only knows the Request and ResponseModel but not the usecase
     * so we use the InputBoundary a UseCase Interface
     *
     * The Controller is the delivery mechanism
     */

    RegisterUserPresenter registerUserPresenter;
    UseCase<RegisterUser.RequestValues, RegisterUser.ResponseValue> useCaseInputBoundary;


    public RegisterUserController(RegisterUserPresenter registerUserPresenter, UseCase<RegisterUser.RequestValues, RegisterUser.ResponseValue> useCaseInputBoundary) {
        this.registerUserPresenter = registerUserPresenter;
        this.useCaseInputBoundary = useCaseInputBoundary;
    }


    public void registerUser(String firstName, String LastName, String birthDate){




    }



}
